## 目录结构

- `api` 接口请求文件，按功能模块分独立文件
- `components` 不包含业务逻辑的公共组件
- `componenst/hooks` 公共钩子函数
- `constants` 常量定义
- `reducers` 全局 reducer, 所有 js 文件会被`store.js`自动引用挂载到 redux 中
- `pages` 页面文件，所有非 common、components 子目录的.jsx 文件会被 `RouteConfig.jsx`自动引用
- `pages/components` 包含业务逻辑的公共组件

#### 样式

webpack 中配置了 CSS Modules， 默认情况下引用 css、less 样式会被识别为 js 对象，声明样式的时候不用担心样式名冲突

引用 global.css、global.less 后缀的样式会全局生效

```css
/* style.css */
.text {
  color: red;
  font-size: 14px;
}
```

```css
/* style.global.css */
.text {
  display: inline-block;
}
```

```jsx
/* component.jsx */
import clsx from 'clsx';
import styles from './styles.css';
import 'styles.global.css';

// 错误示例
function WrongExpample({ text }) {
  // color 和 fonts-size的样式不会生效
  return <span className='text'>{text}</span>;
}

// 正确示例
function TextComponent({ text }) {
  // color、font-size、display样式都生效
  return <span className={clsx('text', styles.text)}>{text}</span>;
}
```

## VS Code 插件

- [CSS Modules](https://marketplace.visualstudio.com/items?itemName=clinyong.vscode-css-modules) css 模块引用的单词联想
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) 代码格式化

## 一些额外的高星库推荐

- [`ahooks`](https://github.com/streamich/react-use) 钩子函数 阿里巴巴 umi 团队维护
- [`react-use`](https://github.com/streamich/react-use) 钩子函数
- [`react-hook-form`](https://react-hook-form.com/) 表单组件和钩子
- [`recoil`](https://recoiljs.org/) 下一代 hook 式状态管理方案

## 知识点文章

- [useEffect 完整指南](https://overreacted.io/zh-hans/a-complete-guide-to-useeffect/)
