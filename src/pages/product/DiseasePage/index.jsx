import DiseaseAPI from '@/api/Disease';
import { Icon, mapField, PaginationTable, SearchFields, useCall, usePaginationCommon } from '@/components';
import { CompanySearchField } from '@/pages/components';
import { pathJoin } from '@/utils';
import { Button } from 'antd';
import React from 'react';

const fields = [
  [['t', 'productCode'], '险种编码'],
  [['t', 'productName'], '险种名称'],
  [['t', 'companyName'], '保险公司', CompanySearchField, { placeholder: '请选择保险公司' }],
].map(mapField);

const columns = [
  { dataIndex: 'companyName', title: '保险公司', width: 110, ellipsis: true },
  { dataIndex: 'productCode', title: '险种编码' },
  { dataIndex: 'productName', title: '险种名称', minWidth: 100 },
  // { dataIndex: 'diseaseType', title: '病种分类' },
];
const actions = {
  edit: (
    <Button size='small' type='text'>
      <Icon type='edit' />
    </Button>
  ),
  del: {
    confirm: '是否确认删除此病种?',
    onOk: (row) => DiseaseAPI.remove(row.productCode),
    text: (
      <Button size='small' type='text'>
        <Icon type='delete' />
      </Button>
    ),
  },
};

function DiseasePage(props) {
  const { tableRef, searchData, onSearch, reset } = usePaginationCommon();

  return (
    <>
      <SearchFields fields={fields} onSubmit={onSearch} onReset={onSearch} />

      {/*<Box flex jus='between' py={2}>
       <div />
       <LinkWithSelectCompany to='edit'>
       <Button>+新增病种</Button>
       </LinkWithSelectCompany>
       </Box>*/}

      <PaginationTable
        ref={tableRef}
        rowKey='productCode'
        service={DiseaseAPI.findByPage}
        serviceData={searchData}
        columns={columns}
        actions={actions}
        onAfterDel={reset}
        onEdit={useCall((row) => props.history.push(pathJoin('edit', { productCode: row.productCode })))}
      />
    </>
  );
}

export default DiseasePage;
