import DiseaseAPI from '@/api/Disease';
import { FieldCols, Icon, mapField, PopoverConfirm, useCall } from '@/components';
import { useSelector, useUpdater } from '@/components/StateProvider';
import { nextTick, pick, simpleMerge, wait } from '@/utils';
import { get, set } from '@/utils/fp';
import { Button, Form, message, Space } from 'antd';
import React, { useContext, useEffect, useRef } from 'react';
import { createItem, getKeyPath, ItemCtx, sortTreeByHasChild } from './shared';

const ItemActions = () => {
  const { parent, onAdd, onRemove } = useContext(ItemCtx);
  return (
    <Space size='large'>
      <PopoverConfirm content='节点删除后不可恢复，确认删除?' onOk={onRemove}>
        <Button>删除节点</Button>
      </PopoverConfirm>

      {parent?.groupBy !== 'diseaseGroup' && (
        <Button icon={<Icon type='plus' />} type='primary' ghost onClick={onAdd}>
          添加子节点
        </Button>
      )}
    </Space>
  );
};

function DiseaseItemEdit() {
  const update = useUpdater();
  const [form] = Form.useForm();

  const { currentKey, keyPath = null, parent = null } = useSelector() ?? {};
  const selected = useSelector((v) => (v?.keyPath && get(v.keyPath)(v.treeList)) ?? null);

  const { groupBy, key, parentKey } = selected || {};

  useEffect(() => {
    nextTick(() => {
      update((v) => {
        const keyPath = getKeyPath(v.treeList, currentKey);
        const parent = get(keyPath.slice(0, -2))(v.treeList);
        return { parent, keyPath };
      });
    });
  }, [currentKey]);

  useEffect(() => {
    if (selected) form.setFieldsValue(selected);
  }, [key]);

  const updateByKeys = (data, keys = keyPath) => {
    update((v) => {
      let treeList = JSON.parse(JSON.stringify(v.treeList));
      set(treeList, keys)(simpleMerge(get(keys)(treeList), data));
      treeList = sortTreeByHasChild(treeList);
      return { treeList };
    });
  };

  const handleChange = useCall(() => {
    const formData = pick(form.getFieldsValue(), [
      'diseaseType',
      'illnessDesc',
      'diseaseGroup',
      'diseaseName',
      'diseaseDesc',
    ])[0];
    updateByKeys({ title: formData[groupBy], ...formData });
  });

  const onAddChild = useCall(async () => {
    const newItem = createItem('diseaseName', key);
    const updateSelected = { children: [...selected.children, newItem] };

    if (groupBy === 'diseaseName') {
      console.log(selected.groupBy, selected[selected.groupBy]);
      updateSelected.groupBy = 'diseaseGroup';
      updateSelected.diseaseGroup = selected.diseaseName;
      updateSelected.title = selected.diseaseName;
    }
    updateByKeys(updateSelected);

    await wait();
    update((v) => ({
      currentKey: newItem.key,
      expandedKeys: (v?.expandedKeys ?? []).concat(key),
    }));
  });

  const onRemove = useCall(async () => {
    if (selected.children?.length) return message.error('请先删除子节点'), void 0;

    if (selected.id) await DiseaseAPI.removeItem(selected.id);

    if (parentKey === '0') {
      update((v) => ({ treeList: v.treeList.filter((t) => t.key !== key) }));
    } else {
      const parentSetterKeys = keyPath.slice(0, keyPath.length - 2);
      const parentUpdate = { children: parent.children.filter((t) => t.key !== key) };
      if (parent.groupBy === 'diseaseGroup' && parentUpdate.children.length === 0) {
        parentUpdate.groupBy = 'diseaseName';
        parentUpdate.title = parent.diseaseName;
      }
      updateByKeys(parentUpdate, parentSetterKeys);
    }

    await wait();
    update({ currentKey: parentKey });

    message.success('删除成功');
  });

  const fields = useRef({
    diseaseType: [
      ['diseaseType', '病种种类'],
      ['illnessDesc', '疾病描述', 'textarea'],
      [null, ' ', ItemActions, { colon: false }],
    ].map(mapField),
    diseaseGroup: [
      ['diseaseGroup', '病种分组'],
      [null, ' ', ItemActions, { colon: false }],
    ].map(mapField),
    diseaseName: [
      ['diseaseName', '病种名称'],
      ['diseaseDesc', '病种描述', 'textarea'],
      [null, ' ', ItemActions, { colon: false }],
    ].map(mapField),
  }).current;

  return (
    <ItemCtx.Provider value={{ selected, parent, onAdd: onAddChild, onRemove }}>
      <Form form={form} onChange={handleChange}>
        <FieldCols form={form} colProps={{ xs: 20 }} fields={fields[groupBy] ?? []} />
      </Form>
    </ItemCtx.Provider>
  );
}

export default DiseaseItemEdit;
