import ProductAPI from '@/api/Product';
import { PaginationTable, SearchFields, useCall, usePaginationCommon } from '@/components';
import { PRODUCT_TYPE_INSURE } from '@/constants/options';
import { PRODUCT_PAGE_FIELDS } from '@/constants/sharedFields';
import { pathJoin, pick } from '@/utils';
import { Button } from 'antd';
import React, { useRef } from 'react';

function AttachmentPage(props) {
  const { tableRef, searchData, onSearch, onSelect, selectedRef, reset } = usePaginationCommon();

  const actions = useRef({
    edit: (
      <Button size='small' type='link'>
        上传
      </Button>
    ),
  }).current;

  const onEdit = useCall((row) => {
    props.history?.push(pathJoin('edit', pick(row, ['id', 'businessCode', 'productType'])[0]));
  });

  const columns = useRef([
    { key: 'order', format: 'order', width: 64 },
    { dataIndex: 'businessCode', title: '业务编码', sorter: true, width: 160 },
    { dataIndex: 'productCode', title: '险种编码', sorter: true, width: 160 },
    { dataIndex: 'companyName', title: '保险公司', width: 110, ellipsis: true },
    { dataIndex: 'productName', title: '险种名称', minWidth: 100, ellipsis: true },
    { dataIndex: 'productCategory', title: '险种分类', dict: 'agency', width: 96 },
    { dataIndex: 'productType', title: '主附险类型', map: PRODUCT_TYPE_INSURE, width: 120 },
  ]).current;

  return (
    <>
      <SearchFields fields={PRODUCT_PAGE_FIELDS} onSubmit={onSearch} onReset={onSearch} />
      <PaginationTable
        ref={tableRef}
        service={ProductAPI.getList}
        serviceData={searchData}
        columns={columns}
        actions={actions}
        actionCol={{ width: 80 }}
        onEdit={onEdit}
      />
    </>
  );
}

export default AttachmentPage;
