import React, { useRef } from 'react';
import FactorAPI from '@/api/Factor';
import { TIME_RISK_MARK, PRODUCT_TYPE_INSURE } from '@/constants/options';
import { PaginationTable, SearchFields, mapField, usePaginationCommon, SelectWithEmpty, useCall, Icon } from '@/components';
import { enumToOptions, pick, pathJoin } from '@/utils';
import { Button } from 'antd';

const fields = [
    [['t', 'productCode'], '险种编码'],
    [['t', 'productName'], '险种名称'],
    [['t', 'timeRiskMark'], '长短险类型', SelectWithEmpty, { options: enumToOptions(TIME_RISK_MARK) }],
    [['t', 'productCategory'], '险种分类', 'dict', { component: SelectWithEmpty, code: 'agency', hides: [''] }],
    [['t', 'productType'], '主附险类型', SelectWithEmpty, { options: enumToOptions(PRODUCT_TYPE_INSURE) }],
].map(mapField);

const InsuranceFile = (props) => {
    const { tableRef, searchData, onSearch, reset } = usePaginationCommon();

    const actions = useRef({
        edit: (
            <Button type='link' icon={<Icon type='edit' />} />
        ),
    }).current;

    const columns = useRef([
        { dataIndex: 'productCode', title: '险种编码' },
        { dataIndex: 'productName', title: '险种名称', ellipsis: true },
        { dataIndex: 'productType', title: '主附险类型', map: PRODUCT_TYPE_INSURE, width: 120 },
    ]).current;

    const onEdit = useCall((row) => {
        console.log(row, '5555')
        props.history?.push(pathJoin('edit', pick(row, ['id'])[0]));
    });

    return (
        <div>
            <SearchFields fields={fields} onSubmit={onSearch} onReset={onSearch} />
            <PaginationTable
                ref={tableRef}
                service={FactorAPI.findByPage}
                serviceData={searchData}
                columns={columns}
                actions={actions}
                actionCol={{ width: 80 }}
                onEdit={onEdit}
            />
        </div>
    )
}
InsuranceFile.title = '险种资料管理';
export default InsuranceFile;