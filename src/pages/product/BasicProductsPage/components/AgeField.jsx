import { FieldTypes, useCall } from '@/components';
import { InputNumber, Select } from 'antd';
import React, { useRef } from 'react';

function AgeField({ value, onChange, selectProps = null, groupProps = null, ...props }) {
  const [age = null, unit = 'A'] = (value ?? '').split('_');
  const options = useRef([
    { value: 'A', label: '岁' },
    { value: 'D', label: '天' },
  ]).current;

  const ageChange = useCall((val) => {
    onChange?.(val ? `${val}_${unit}` : null);
  });

  const unitChange = useCall((unit) => {
    onChange?.(age ? `${age ?? ''}_${unit}` : null);
  });

  return (
    <FieldTypes.group compact>
      <InputNumber {...props} value={age} onChange={ageChange} />
      <Select {...selectProps} options={options} value={unit} onChange={unitChange} />
    </FieldTypes.group>
  );
}

export default AgeField;
