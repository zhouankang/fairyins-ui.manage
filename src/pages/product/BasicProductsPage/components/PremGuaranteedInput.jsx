import { Box, useCall, useSimpleMemo } from '@/components';
import { INPUT_KEYS } from '@/constants/componentKeys';
import { pick } from '@/utils';
import { Input, InputNumber, Select } from 'antd';
import React from 'react';

const options = [
  { value: 'Y', label: '年' },
  { value: 'A', label: '岁' },
  { value: 'D', label: '天' },
];

function PremGuaranteedInput(props) {
  const { value, onChange, ...rest } = props;
  const [number, unit = 'Y'] = value?.split('_') ?? [];

  const [inputProps, otherProps] = useSimpleMemo(pick(rest, INPUT_KEYS));

  const handleChangeNum = useCall((e) => {
    onChange(`${e || ''}_${unit}`);
  });

  const handleChangeUnit = useCall((e) => {
    onChange(`${number || ''}_${e}`);
  });

  return (
    <Box {...otherProps} component={Input.Group} flex dir='row' compact>
      <Box {...inputProps} component={InputNumber} grow min={0} value={number || ''} onChange={handleChangeNum} />
      <Select options={options} value={unit} onChange={handleChangeUnit} />
    </Box>
  );
}

export default PremGuaranteedInput;
