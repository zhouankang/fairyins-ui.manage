import { createContext } from 'react';

export const Context = createContext({ form: null, productCode: null });

export * from './shared';

export { default as PremGuaranteedInput } from './PremGuaranteedInput';
export { default as ExemptionsSelect } from './ExemptionsSelect';
export { default as AgeField } from './AgeField';
export { default as AttributePoolFields } from './AttributePoolFields';
export { default as DutyFields } from './DutyFields';
