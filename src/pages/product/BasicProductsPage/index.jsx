import ProductAPI from '@/api/Product';
import { Box, Icon, PaginationTable, SearchFields, useCall, usePaginationCommon } from '@/components';
import { PRODUCT_TYPE_INSURE } from '@/constants/options';
import { PRODUCT_PAGE_FIELDS } from '@/constants/sharedFields';
import { LinkWithSelectCompany, SelectCompany } from '@/pages/components';
import { pathJoin } from '@/utils';
import { Button, Space, Tooltip } from 'antd';
import React from 'react';

const FIELDS = PRODUCT_PAGE_FIELDS;

const COLUMNS = [
  { key: 'index', format: 'order', width: 64 },
  {
    dataIndex: 'businessCode',
    title: '险种编码',
    link: 'addBasicProducts?action=edit',
    sorter: true,
    width: 110,
  },
  // { dataIndex: 'productCode', title: '险种编码', sorter: true, width: 110 },
  // { dataIndex: 'companyName', title: '保险公司', width: 110, ellipsis: true },
  { dataIndex: 'productName', title: '险种名称', minWidth: 160, ellipsis: true },
  { dataIndex: 'productCategory', title: '险种分类', width: 'calc(4.2em + 32px)', dict: 'agency' },
  {
    dataIndex: 'productType',
    title: '主附险类型',
    width: 'calc(5.2em + 32px)',
    map: PRODUCT_TYPE_INSURE,
  },
  { dataIndex: '', title: '险种是否更新', minWidth: 160, ellipsis: true },
  { dataIndex: '', title: '同步时间', minWidth: 160, ellipsis: true },
];

const actions = {
  disease: (row) =>
    row.productCategory === 'MI' && (
      <Button type='link' size='small'>
        病种
      </Button>
    ),
  synchronization: (row) => (
    <Button type='link' size='small'>
      同步
    </Button>
  ),
  synchronousRecording: (row) => (
    <Button type='link' size='small'>
      同步记录
    </Button>
  )
  // copy: {
  //   dialog: SelectCompany,
  //   text: (
  //     <Button type='link' size='small'>
  //       <Tooltip title='复制险种'>
  //         <Icon type='copy' />
  //       </Tooltip>
  //     </Button>
  //   ),
  // },
  // concat: (
  //   <Button type='link' size='small'>
  //     <Tooltip title='查看关联关系'>
  //       <Icon type='link' />
  //     </Tooltip>
  //   </Button>
  // ),
  // attachment: (
  //   <Button type='link' size='small'>
  //     <Tooltip title='附件管理'>
  //       <Icon type='download' />
  //     </Tooltip>
  //   </Button>
  // ),

};

function BasicProductsPage({ history }) {
  const { tableRef, searchData, onSearch, onSelect, selectedRef, reset } = usePaginationCommon();

  const onDisease = useCall(({ productCode, productName, companyName }) => {
    history.push('../DiseasePage/edit' + pathJoin({ productCode, productName, companyName }));
  });
  const onSynchronization = useCall((row) => {
    console.log(row, 'row')
  })
  const onSynchronousRecording = useCall((row) => {
    console.log(row, 'row')
  })

  // const onCopy = useCall(({ productCode, businessCode }, index, [selection] = []) => {
  //   const { code: companyCode = null, name: companyName = null } = selection ?? {};

  //   history.push(pathJoin('addBasicProducts', { action: 'copy', productCode, businessCode, companyName, companyCode }));
  // });
  // const onConcat = useCall((row) => {
  //   sessionStorage.setItem('relationEditItem', JSON.stringify(row));
  //   const { businessCode, productType } = row;
  //   history.push('../ContactPage/addPage' + pathJoin({ businessCode, productType }));
  // });
  // const onAttachment = useCall(({ id, businessCode, productType }) => {
  //   history.push('../AttachmentPage/edit' + pathJoin({ id, businessCode, productType }));
  // });




  return (
    <>
      <SearchFields fields={FIELDS} onSubmit={onSearch} onReset={onSearch} />

      <Box flex jus='between' py={2}>
        <Space>
          <div />
        </Space>
        <Space size='middle'>
          <LinkWithSelectCompany to='addBasicProducts?action=create'>
            <Button>+新增险种</Button>
          </LinkWithSelectCompany>
        </Space>
      </Box>

      <PaginationTable
        ref={tableRef}
        service={ProductAPI.getList}
        serviceData={searchData}
        columns={COLUMNS}
        actions={actions}
        actionCol={{ width: 180 }}
        onDisease={onDisease}
        onSynchronization={onSynchronization}
        onSynchronousRecording={onSynchronousRecording}
      // onCopy={onCopy}
      // onAttachment={onAttachment}
      // onConcat={onConcat}
      // rowSelection={{
      //   onChange: onSelect,
      // }}
      />
    </>
  );
}

export default BasicProductsPage;
