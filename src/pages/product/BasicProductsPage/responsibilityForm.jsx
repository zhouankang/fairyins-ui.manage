

import ProductAPI from '@/api/Product';
import { FieldCols, FormSection, useCall, useFormValue, useQueryValue, useReq } from '@/components';
import { getQueryValues, pathJoin } from '@/utils';
import { catchAndScrollFor } from '@/utils/fp';
import { Form, message, Space, Spin } from 'antd';
import React, { useEffect, useMemo, useRef } from 'react';
import { Context, data2Form, form2Data } from './components';
import * as FIELDS from './components/FIELDS';

function Provider({ form, children }) {
    const productCode = useFormValue(form, ['productAddVo', 'productAttrVo', 'productCode']);

    const ctx = useMemo(() => ({ form, productCode }), [productCode]);

    return <Context.Provider value={ctx}>{children}</Context.Provider>;
}

function ResponsibilityForm(props) {
    const businessCode = useQueryValue('businessCode');
    const productCode = useQueryValue('productCode');
    const isCopy = useQueryValue('action') === 'copy';


    const [form] = Form.useForm();
    const originalDataRef = useRef(null);
    const xhr = useReq(ProductAPI.getInfo);
    const copyXhr = useReq(ProductAPI.getCopy);
    const saveXhr = useReq(ProductAPI.insertOrUpdate);




    useEffect(() => {
        if (isCopy) {
            copyXhr.start(productCode, businessCode);
        } else if (businessCode) {
            xhr.start(businessCode);
        } else {
            form.resetFields();
        }
    }, []);

    useEffect(() => {
        if (copyXhr.result?.data) {
            originalDataRef.current = JSON.parse(JSON.stringify(copyXhr.result.data));
            const [companyCode, companyName] = getQueryValues(location.search, 'companyCode', 'companyName');
            const formData = data2Form(copyXhr.result.data);
            if (companyCode) {
                formData.productAddVo.productAttrVo.companyCode = companyCode;
                formData.productAddVo.productAttrVo.companyName = companyName;
            }
            form.setFieldsValue(formData);
        }
    }, [copyXhr.result]);

    useEffect(() => {
        if (xhr.result?.data) {
            originalDataRef.current = JSON.parse(JSON.stringify(xhr.result.data));
            form.setFieldsValue(data2Form(xhr.result.data));
        }
    }, [xhr.result]);

    const handleSave = useCall(async () => {
        console.log(props,'props')
        props.onResponsibilityOk(1233)
        // const values = await form.validateFields().catch(catchAndScrollFor(form));
        // const data = form2Data(values, originalDataRef.current);
        // await saveXhr.start(data);
        // message.success('保存成功');
        // history.replace(pathJoin(location.pathname, '../'));
    });

    const loading = [xhr.status, copyXhr.status, saveXhr.status].includes('loading');

    return (
        <Provider form={form}>
            <Spin spinning={loading}>
                <Form form={form}>
                    <FormSection title='基本信息'>
                        <FieldCols form={form} fields={FIELDS.basic} />
                    </FormSection>

                    <FormSection title='投保条件'>
                        <FieldCols form={form} fields={FIELDS.condition} />
                    </FormSection>
                </Form>
            </Spin>
        </Provider>
    );
}

export default ResponsibilityForm;
