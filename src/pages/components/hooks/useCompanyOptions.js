import { createSharedHook, useReq } from '@/components';
import { useEffect, useMemo } from 'react';

let globalXhr;
export const resetCompanyOptions = () => globalXhr?.start();

const useCompanyOptions = createSharedHook(() => {
  const xhr = useReq({
    url: '../upp/upp/company/getAll',
    method: 'GET',
  });

  xhr.startOnce();

  useEffect(() => {
    globalXhr = xhr;
    return () => {
      globalXhr = null;
    };
  }, []);

  return useMemo(() => {
    return [{ value: '0', label: '通用' }].concat(
      xhr.result?.data?.data?.map((v) => ({
        value: v.code,
        label: v.name,
      })) ?? [],
    );
  }, [xhr.result]);
});

export const companyOptionsTableRender = (col) => {
  const options = useCompanyOptions();
  return col ? options?.find((v) => v.value === col)?.label ?? col : '--';
};

export default useCompanyOptions;
