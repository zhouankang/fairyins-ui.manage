import FactorAPI from '@/api/Factor';
import { ModalSelectTable } from '@/components';
import { FACTOR_COLUMNS, FACTOR_SEARCH_FIELDS } from '@/constants/sharedFields';
import React, { forwardRef } from 'react';

SelectFactor = forwardRef(SelectFactor);

function SelectFactor(props, ref) {
  return (
    <ModalSelectTable
      ref={ref}
      title='因子库'
      selectType='checkbox'
      {...props}
      service={FactorAPI.findByPage}
      fields={FACTOR_SEARCH_FIELDS}
      columns={FACTOR_COLUMNS}
    />
  );
}

export default SelectFactor;
