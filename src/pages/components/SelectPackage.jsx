import PackageAPI from '@/api/Package';
import { ModalSelectTable } from '@/components';
import { PACKAGE_COLUMNS, PACKAGE_FIELDS } from '@/constants/sharedFields';
import React, { forwardRef } from 'react';

SelectPackage = forwardRef(SelectPackage);

function SelectPackage(props, ref) {
  return (
    <ModalSelectTable
      title='组合'
      {...props}
      service={PackageAPI.listForOnSale}
      rowKey='packageCode'
      fields={PACKAGE_FIELDS}
      columns={PACKAGE_COLUMNS}
    />
  );
}

export default SelectPackage;
