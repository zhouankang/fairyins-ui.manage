import LiabilityAPI from '@/api/Liability';
import { mapField, ModalSelectTable } from '@/components';
import React, { forwardRef } from 'react';

const COLUMNS = [
  { dataIndex: 'liabilityCode', title: '责任代码' },
  { dataIndex: 'liabilityName', title: '责任名称' },
];

const FIELDS = [
  ['liabilityCode', '责任代码'],
  ['liabilityName', '责任名称'],
].map(mapField);

SelectLiability = forwardRef(SelectLiability);

function SelectLiability(props, ref) {
  return (
    <ModalSelectTable
      ref={ref}
      title='选择责任'
      {...props}
      columns={COLUMNS}
      fields={FIELDS}
      service={LiabilityAPI.findByPage}
      onService={(fieldsValue) => [{ pageSize: 9999, t: fieldsValue }]}
    />
  );
}

export default SelectLiability;
