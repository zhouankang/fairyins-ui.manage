import { useWithinEmptyOptions } from '@/components';
import { notNull } from '@/utils';
import { Select } from 'antd';
import React, { useMemo } from 'react';
import useCompanyOptions from './hooks/useCompanyOptions';

/** 选择保险公司下拉框组件 */
function CompanySelectField(props) {
  const data = useCompanyOptions();

  const options = useMemo(() => {
    return data?.map((v) => ({ value: v.value, label: v.label }));
  }, [data]);

  return (
    <Select
      options={useWithinEmptyOptions(options)}
      optionFilterProp='label'
      showSearch
      {...props}
      value={props.value ?? null}
      allowClear={notNull(props.value)}
    />
  );
}

export default CompanySelectField;
