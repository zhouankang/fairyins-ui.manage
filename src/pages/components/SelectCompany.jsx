import { mapField, ModalSelectTable } from '@/components';
import React, { forwardRef } from 'react';

function SelectCompany(props, ref) {
  return (
    <ModalSelectTable
      ref={ref}
      title='选择保险公司'
      width={600}
      {...props}
      service={{
        url: '../upp/upp/company/getAll',
        method: 'GET',
      }}
      onResult={(r) => r?.data?.data ?? []}
      rowKey='code'
      fields={[
        [
          ['params', 'search_content'],
          '查询',
          null,
          {
            itemProps: {
              labelAlign: 'left',
              labelCol: { flex: 'none' },
              wrapperCol: { flex: 'auto' },
            },
          },
        ],
      ].map(mapField)}
      columns={[
        {
          key: 'seq',
          format: 'order',
          width: 64,
          title: '序号',
        },
        {
          dataIndex: 'name',
          title: '公司名称',
        },
      ]}
    />
  );
}

SelectCompany = forwardRef(SelectCompany);

export default SelectCompany;
