import FormulaAPI from '@/api/Formula';
import { ModalSelectTable } from '@/components';
import { FORMULA_COLUMNS, FORMULA_SEARCH_FIELDS } from '@/constants/sharedFields';
import React, { forwardRef } from 'react';

SelectFormula = forwardRef(SelectFormula);

function SelectFormula(props, ref) {
  return (
    <ModalSelectTable
      ref={ref}
      title='子公式'
      {...props}
      selectType='radio'
      service={FormulaAPI.findByPage}
      fields={FORMULA_SEARCH_FIELDS}
      columns={FORMULA_COLUMNS}
    />
  );
}

export default SelectFormula;
