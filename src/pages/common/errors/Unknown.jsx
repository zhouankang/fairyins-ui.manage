import React from 'react';

export default ({ message, stack, componentStack, onClose }) => (
  <div className='app-error-overlay'>
    <h3>
      {message ?? 'Unknown Error.'}
      <a href='javascript: void 0' className='app-overlay-close' onClick={onClose}>
        ×
      </a>
    </h3>
    <pre>{`Stack: ${stack ?? null}\nComponent stack: ${componentStack ?? null}`}</pre>
  </div>
);
