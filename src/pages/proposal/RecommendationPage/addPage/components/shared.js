import { useRaf } from '@/components';
import { optionsToMap } from '@/utils';
import { useRef, useState } from 'react';
import { findDOMNode } from 'react-dom';

export const SIGN_VALUES = [
  { value: null, label: '无' },
  { value: 'LOW', label: '低档位' },
  { value: 'SECONDARY', label: '中档位' },
  { value: 'HIGHER', label: '高档位' },
  { value: 'tab', label: '页签' },
];

export const SIGN_MAP = optionsToMap(SIGN_VALUES);

export const ALIAS_VALUES = [
  { value: null, label: '无', desc: '(显示为金额或文本)' },
  { value: 'age', label: '年龄', desc: '(显示为“N岁”)' },
  { value: 'policy', label: '年度', desc: '(显示为“第N年”)' },
  // { value: 'wan', label: '万元', desc: '(金额大于10万显示为“N万元”)' },
];

export const ALIAS_MAP = optionsToMap(ALIAS_VALUES);

export const useTableHeight = (defaultHeight = 120) => {
  const ref = useRef();
  const [tableHeight, setTableHeight] = useState(defaultHeight);

  useRaf(() => {
    setTableHeight(ref.current ? findDOMNode(ref.current).offsetHeight : defaultHeight);
  });

  return [ref, tableHeight];
};
