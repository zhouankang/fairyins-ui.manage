import { ClickModal, FieldCols, mapField, useCall } from '@/components';
import { forkHandler } from '@/utils';
import { catchAndScrollFor } from '@/utils/fp';
import { Form } from 'antd';
import * as PropTypes from 'prop-types';
import React, { forwardRef } from 'react';
import { ALIAS_VALUES, SIGN_VALUES } from './shared';

const COMMON = [
  ['benefitName', '利益名称', null, { required: true }],
  [
    'benefitCode',
    '利益代码',
    null,
    {
      rules: [
        { required: true, message: '请输入利益代码' },
        { pattern: /^[^_]*$/, message: '利益代码不能输入下划线' },
      ],
    },
  ],
];

const DATA_FIELDS = [
  ...COMMON,
  /*[
   'isShow',
   '字段可见性',
   'radio-group',
   {
   initialValue: '1',
   options: [
   { value: '1', label: '显示' },
   { value: '0', label: '不显示' },
   ],
   },
   ],*/
  ['isShow', '字段可见性', null, { required: true, extra: '支持规则输入, (1 展示； 0 不展示)' }],
  ['benefitFormula', '利益公式'],
  ['alias', '内容类型', 'select', { initialValue: null, options: ALIAS_VALUES }],
  ['sign', '字段标记', 'select', { initialValue: null, options: SIGN_VALUES }],
  ['factorFormula', '定义变量名', null, { extra: '辅助利益演算的计算；例如 {“XXX”：”’tr’+tr+’td’+(td-1)”}' }],
].map(mapField);

const DESC_FIELDS = [
  ...COMMON,
  ['isShow', '字段可见性', null, { required: true, extra: '支持规则输入, (1 展示； 0 不展示)' }],
  [
    'benefitDesc',
    '利益描述',
    'editor',
    {
      type: 'simple',
      colProps: { xs: 24 },
      itemProps: { labelCol: { span: 24 }, wrapperCol: { span: 24 } },
    },
  ],
].map(mapField);

const fieldProps = {
  0: {
    colProps: { xs: 12 },
    wrapperSpan: 18,
    fields: DESC_FIELDS,
  },
  1: {
    colProps: { xs: 24 },
    wrapperSpan: 12,
    fields: DATA_FIELDS,
  },
};

BenefitDataEditModal = forwardRef(BenefitDataEditModal);

function BenefitDataEditModal(props, ref) {
  const {
    parent = null,
    sibling = null,
    item = null,
    type = 0,
    resetValue = null,
    index,
    onOk,
    onToggle,
    ...rest
  } = props;
  const [form] = Form.useForm();

  const handleOk = useCall(async () => {
    const values = await form.validateFields().catch(catchAndScrollFor(form));
    let mergeItem = item;
    if (parent) {
      mergeItem = {
        type: parent.type,
        planCode: parent.planCode,
        productCode: parent.productCode,
        on: index + 1,
        layer: parent.layer + 1,
        parentCode: parent.benefitCode,
      };
    }

    if (sibling) {
      mergeItem = {
        type: sibling.type,
        planCode: sibling.planCode,
        productCode: sibling.productCode,
        parentCode: sibling.parentCode,
        layer: sibling.layer,
        on: index + 1,
      };
    }

    return onOk?.({
      ...mergeItem,
      ...values,
    });
  });

  const handleToggle = useCall(
    forkHandler(onToggle, (visi) => {
      if (visi) resetValue ? form.setFieldsValue(resetValue) : item ? form.setFieldsValue(item) : form.resetFields();
    }),
  );

  const content = (
    <Form form={form}>
      <FieldCols {...fieldProps[type]} />
    </Form>
  );
  return (
    <ClickModal ref={ref} title='利益演示配置' {...rest} onOk={handleOk} onToggle={handleToggle} content={content} />
  );
}

BenefitDataEditModal.propTypes = {
  parent: PropTypes.any,
  sibling: PropTypes.any,
  item: PropTypes.any,
  type: PropTypes.number,
  resetValue: PropTypes.any,
  index: PropTypes.any,
  onOk: PropTypes.any,
  onToggle: PropTypes.any,
};

export default BenefitDataEditModal;
