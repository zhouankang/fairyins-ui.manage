import ProposalAPI from '@/api/Proposal';
import {
  Box,
  FieldCols,
  Icon,
  mapField,
  PaginationTable,
  PopoverConfirm,
  SearchFields,
  useCall,
  usePaginationCommon,
} from '@/components';
import { PRODUCT_TYPE_INSURE } from '@/constants/options';
import { forkHandler } from '@/utils';
import { Button, Form } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

const FIELDS = [
  [['searchFuzzy', 'product_code'], '险种编码'],
  [['searchFuzzy', 'product_name'], '险种名称'],
  [
    ['sectionStartParams', 'create_time'],
    '创建日期',
    'named-range',
    { endName: ['../', 'sectionEndParams', 'create_time'] },
  ],
].map(mapField);

const COLUMNS = [
  { dataIndex: 'productCode', title: '险种编码', sorter: true },
  { dataIndex: 'productName', title: '险种名称', minWidth: 100 },
  { dataIndex: 'productType', title: '主附险类型', map: PRODUCT_TYPE_INSURE },
  { dataIndex: 'createTime', title: '创建日期', sorter: true, defaultSortOrder: 'descend' },
];

const ACTIONS = {
  edit: (row) => (
    <Link to={`addPage/?code=${row.id}`}>
      <Button type='link' icon={<Icon type='edit' />} />
    </Link>
  ),
  del: {
    confirm: '确认删除此建议书配置吗?',
    text: <Button type='link' icon={<Icon type='delete' />} />,
  },
};

function RecommendationPage() {
  const { tableRef, searchData, onSearch, onSelect, selectedRef, hasSelected, reset } = usePaginationCommon();

  const onBatchDel = useCall(forkHandler(() => ProposalAPI.remove(selectedRef.current), reset, true));
  const onDel = useCall(forkHandler((row) => ProposalAPI.remove([row.id]), reset, true));

  return (
    <>
      <SearchFields fields={FIELDS} onSubmit={onSearch} onReset={onSearch} />
      <Box flex jus='between' py={2}>
        {hasSelected && (
          <PopoverConfirm content='确认删除已选中的建议书配置吗?' onOk={onBatchDel}>
            <Button>批量删除</Button>
          </PopoverConfirm>
        )}

        <span />

        <Link to='addPage/'>
          <Button>新增建议书11</Button>
        </Link>
      </Box>
      <PaginationTable
        ref={tableRef}
        service={ProposalAPI.findByPage}
        serviceData={searchData}
        columns={COLUMNS}
        actions={ACTIONS}
        actionWidth={100}
        rowSelection={{
          onChange: onSelect,
        }}
        onDel={onDel}
      />
    </>
  );
}

export default RecommendationPage;
