import React, { useRef } from 'react';
import FactorAPI from '@/api/Factor';
import { PaginationTable, usePaginationCommon, useCall, Icon } from '@/components';
import { enumToOptions, pick, pathJoin } from '@/utils';
import { Button, Upload } from 'antd';

const Public = (props) => {
    const { tableRef, searchData } = usePaginationCommon();

    const UploadProps = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            console.log(info, 'info')
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success('上传成功');
                // message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.success('上传失败');
            }
        },
    };

    const handleUpload = (row) => {
        console.log('点击上传', row)
    }

    const handleDownload = (row) => {
        console.log('点击上传', row)
    }

    const columns = useRef([
        { dataIndex: 'productCode', title: '复星保德信人寿保险公司' },
        { dataIndex: 'productName', title: '类型', ellipsis: true },
        { dataIndex: 'productType', title: '用途' },
        {
            dataIndex: '', title: '操作', render(col, row, index) {
                return (
                    <div style={{ display: "flex" }}>
                        {/* 'pdf' */}
                        <Upload {...UploadProps} accept="image/*" fileList={[]}>
                            <Button size='small' type='primary' style={{ marginRight: '10px' }} onClick={() => handleUpload(row)}>
                                上传
                            </Button>
                        </Upload>
                        <Button size='small' type='primary' onClick={() => handleDownload(row)} disabled>
                            下载
                        </Button>
                    </div>
                )
            }
        },
    ]).current;



    return (
        <div>
            <PaginationTable
                ref={tableRef}
                service={FactorAPI.findByPage}
                serviceData={searchData}
                columns={columns}
                actionCol={{ width: 80 }}
            />
        </div>
    )
}
Public.title = '公共管理';
export default Public;