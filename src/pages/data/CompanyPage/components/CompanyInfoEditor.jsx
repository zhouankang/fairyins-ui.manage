import { useCall } from '@/components';
import DescEditor from '@/pages/Comparison/CompareProduct/components/DescEditor';
import React from 'react';

function CompanyInfoEditor(props) {
  const { value, onChange, maxLength, ...rest } = props;
  const length = value?.length ?? 0;

  const handleChange = useCall((value) => {
    if (value?.length <= maxLength) onChange?.(value);
  });

  return (
    <div style={{ position: 'relative', minHeight: 48, marginLeft: -8 }}>
      <DescEditor value={value} onChange={handleChange} {...rest} editing />
      <div
        style={{
          position: 'absolute',
          right: 0,
          bottom: 0,
          whiteSpace: 'nowrap',
          fontSize: 12,
          padding: 6,
          zIndex: 1,
          color: '#999',
        }}
      >
        {length}/{maxLength}
      </div>
    </div>
  );
}

export default CompanyInfoEditor;
