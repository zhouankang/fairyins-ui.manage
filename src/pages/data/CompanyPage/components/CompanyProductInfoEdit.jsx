import { ClickModal, FieldCols, useCall } from '@/components';
import { catchAndScrollFor } from '@/utils/fp';
import { Form } from 'antd';
import React, { useEffect, useRef } from 'react';
import { dataToForm, fields, formToData } from './shared';

function CompanyProductInfoEdit({ item, onOk, ...props }) {
  const [form] = Form.useForm();
  const formDataRef = useRef({});

  useEffect(() => {
    if (item) {
      const response = dataToForm(item);
      const { CF, SF, PP, ...data } = response;
      formDataRef.current = data;
      console.log({ CF, SF, PP });
      form.setFieldsValue({ CF, SF, PP });
    }
  }, [item]);

  const handleOk = useCall(async () => {
    const formData = await form.validateFields().catch(catchAndScrollFor(form));
    const data = formToData({ ...formDataRef.current, ...formData }, item);

    return onOk?.(data);
  });

  const content = (
    <Form form={form}>
      <FieldCols
        form={form}
        fields={fields.service}
        colProps={{ xs: 20, md: 20, xl: 20 }}
        rowProps={{ justify: 'center' }}
      />
    </Form>
  );

  return <ClickModal title='运营服务' {...props} content={content} onOk={handleOk} />;
}

export default CompanyProductInfoEdit;
