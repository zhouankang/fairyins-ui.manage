import ProductCompanyAPI from '@/api/ProductCompany';
import { Box, Icon, mapField, PaginationTable, SearchFields, useCall, usePaginationCommon } from '@/components';
import { forkHandler } from '@/utils';
import { Button } from 'antd';
import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
import CompanyProductInfoEdit from './components/CompanyProductInfoEdit';
import { formToData } from './components/shared';

function CompanyPage(props) {
  const { tableRef, searchData, onSearch, onSelect, selectedRef, reset } = usePaginationCommon();

  const columns = useRef([
    { dataIndex: 'nameChn', title: '公司名称', width: 160, ellipsis: true },
    { dataIndex: 'createDate', title: '成立时间', width: 100, props: { style: { fontSize: 12 } } },
    { dataIndex: 'comNature', title: '公司性质', width: 120, ellipsis: true },
    { dataIndex: 'registeredCapital', title: '注册资本', width: 100, ellipsis: true },
    { dataIndex: 'peoControl', title: '实际控制人', width: 100, ellipsis: true },
    { dataIndex: 'sarmra', title: 'SARMARA评分', width: 100, ellipsis: true },
    { dataIndex: 'cliaimTime', title: '理赔时效', width: 100, ellipsis: true },
    { dataIndex: 'cliaimMoney', title: '理赔总额', width: 100, ellipsis: true },
  ]).current;

  const fields = useRef(
    [
      [['t', 'nameChn'], '公司名称'],
      [['t', 'comNature'], '公司性质' /*, 'select', { allowClear: true, options: natureTypes, showSearch: true }*/],
    ].map(mapField),
  ).current;

  const actions = useRef({
    edit: (
      <Button type='link' size='small'>
        <Icon type='edit' />
      </Button>
    ),
    upload: {
      dialog: CompanyProductInfoEdit,
      itemProp: 'item',
      text: (
        <Button type='link' size='small'>
          上传
        </Button>
      ),
      onOk(r, i, data) {
        return ProductCompanyAPI.insertOrUpdate(data).then(() => {
          reset();
        });
      },
    },
    del: {
      confirm: '是否确认删除？',
      text: (
        <Button type='link' danger size='small'>
          <Icon type='delete' />
        </Button>
      ),
    },
  }).current;

  const onEdit = useCall((row) => {
    sessionStorage.setItem('EditCompanyInfo', JSON.stringify(row));
    props.history?.push('edit?action=edit');
  });
  // const onDel = useCall((row) => ProductCompanyAPI.remove(row));
  const onDel = useCall(forkHandler((row) => ProductCompanyAPI.remove(row), reset, true));

  return (
    <>
      <SearchFields fields={fields} onSubmit={onSearch} onReset={onSearch} />

      <Box flex jus='between' py={2}>
        <div />
        <Link to='edit?action=create'>
          <Button>+新增保险公司</Button>
        </Link>
      </Box>

      <PaginationTable
        ref={tableRef}
        service={ProductCompanyAPI.getList}
        serviceData={searchData}
        columns={columns}
        actions={actions}
        actionCol={{ width: 110 }}
        onEdit={onEdit}
        onDel={onDel}
      />
    </>
  );
}

export default CompanyPage;
