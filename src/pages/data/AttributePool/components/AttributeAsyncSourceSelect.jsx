import { useCall, useFieldCtx } from '@/components';
import { forkHandler, nextTick } from '@/utils';
import { Select } from 'antd';
import React from 'react';
import { attributeRequestMap, attributeSourceOptions } from './shared';

function AttributeAsyncSourceSelect(props) {
  const form = useFieldCtx('form');
  const onFetchData = useCall((value) => {
    if (value) attributeRequestMap[value]?.(form);
  });
  const onChange = useCall(forkHandler(props.onChange, onFetchData));

  const onClear = useCall(() => {
    nextTick(() => {
      onChange(null);
    });
  });

  return <Select {...props} allowClear options={attributeSourceOptions} onChange={onChange} onClear={onClear} />;
}

export default AttributeAsyncSourceSelect;
