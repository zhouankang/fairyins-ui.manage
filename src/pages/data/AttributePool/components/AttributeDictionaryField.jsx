import { useCall, useFieldCtx, useFormValue } from '@/components';
import ControlledEditor from '@monaco-editor/react';
import React from 'react';

function AttributeDictionaryField(props) {
  const { value: defaultValue, onChange, ...rest } = props;
  const form = useFieldCtx('form');
  const readOnly = useFormValue(form, 'attributeSource', (v) => !!v);

  return (
    <ControlledEditor
      height='200px'
      className='field-border'
      defaultLanguage='json'
      options={{
        minimap: {
          enabled: false,
        },
      }}
      defaultValue={defaultValue}
      {...props}
      onChange={useCall((value) => {
        const result = readOnly ? defaultValue : value;
        onChange?.(result);
        return result;
      })}
    />
  );
}

export default AttributeDictionaryField;
