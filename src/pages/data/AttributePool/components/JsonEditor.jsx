import { ControlledEditor } from '@monaco-editor/react';
import React from 'react';

function JsonEditor(props) {
  return <ControlledEditor {...props} />;
}

export default JsonEditor;
