import { pick } from '@/utils';
import Axios from 'axios';

export const attributeSourceOptions = [
  {
    value: null,
    label: '无',
  },
  {
    value: '../upp/upp/company/listDataBase',
    label: 'upp字典',
  },
];

export const attributeRequestMap = {
  async '../upp/upp/company/listDataBase'(form) {
    const { companyCode, attributeCode } = form.getFieldsValue();
    return Axios({
      baseURL: process.env.BASE_URL,
      url: '../upp/upp/company/listDataBase',
      params: { companyCode, codeType: attributeCode },
    })
      .then(
        (res) => res?.data?.data?.map((v) => pick(v, ['code', 'name'])[0]) ?? null,
        () => null,
      )
      .then((data) => {
        if (data) form.setFieldsValue({ attributeDictionary: JSON.stringify(data, null, 2) });
        return data;
      });
  },
};
