import DictionaryAPI from '@/api/Dictionary';
import { useReq } from '@/components';

export function useRootDictList() {
  const xhr = useReq(DictionaryAPI.getRoot);
  xhr.startOnce();

  return [xhr.result ?? [], xhr.status === 'loading'];
}

export const rootIdProps = {
  showSearch: true,
  filterOption: (v, option) => option.label.indexOf(v.toLowerCase()) >= 0,
  getFieldProps() {
    const [result, loading] = useRootDictList() ?? [];
    const options =
      result?.map?.((v) => ({
        value: v.code ? v.id : null,
        label: `${v.cnName} ${v.code}`,
      })) ?? [];
    return { options, loading };
  },
};

export const DICTIONARY_BASE_ROUTE = '/product/data/Dictionary/';
