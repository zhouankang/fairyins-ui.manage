import DictionaryAPI from '@/api/Dictionary';
import { PaginationTable, useCall, useCreateCall } from '@/components';
import { pathJoin } from '@/utils';
import React, { forwardRef } from 'react';
import { useHistory } from 'react-router-dom';
import { DICTIONARY_BASE_ROUTE } from './shared';

const COLUMNS = [
  { dataIndex: 'code', title: '字典编号', width: '20%', ellipsis: true },
  { dataIndex: 'cnName', title: '字典名称', width: '20%', ellipsis: true },
  { dataIndex: 'value', title: '字典值', ellipsis: true },
  { dataIndex: 'enabled', title: '是否启用', width: 96, map: { 1: '是', 0: '否' }, fallback: '否' },
];

const ACTIONS = {
  edit: '编辑',
  add: '新增子节点',
  /*del: {
   confirm: '该节点下的所有子节点也会同步删除，是否确认删除?',
   text: '删除',
   onOk: (row) => DictionaryAPI.remove(row.id),
   },*/
};

DictionaryTable = forwardRef(DictionaryTable);

function DictionaryTable(props, ref) {
  const history = useHistory();
  const createCall = useCreateCall();
  const onActionProps = useCall((type) => ({
    type: 'link',
    danger: type === 'del',
    onClick: createCall((row) => {
      if (type === 'del') return true;
      sessionStorage.setItem(`DICT_NAME:${row.id}`, row.cnName);
      history.push(pathJoin(DICTIONARY_BASE_ROUTE, type, { id: row.id }));
    }),
  }));

  return (
    <PaginationTable
      ref={ref}
      size='middle'
      service={DictionaryAPI.findByPage}
      {...props}
      columns={COLUMNS}
      actions={ACTIONS}
      actionWidth={256}
      onActionProps={onActionProps}
    />
  );
}

export default DictionaryTable;
