import { Box, mapField, SearchFields, usePaginationCommon } from '@/components';
import { Button } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import DictionaryTable from './components/DictionaryTable';
import { rootIdProps } from './components/shared';

const FIELDS = [
  [['t', 'code'], '字典代码(完整)', null, { getValueFromEvent: (v) => v?.target?.value || null }],
  [['searchFuzzy', 'code'], '字典代码(模糊)'],
  [['searchFuzzy', 'cn_name'], '字典名称'],
  [['t', 'parentId'], '父级字典', 'select', rootIdProps],
].map(mapField);

function DictionaryPage() {
  const { tableRef, searchData, onSearch, onSelect, selectedRef, reset } = usePaginationCommon();

  return (
    <>
      <SearchFields fields={FIELDS} onSubmit={onSearch} onReset={onSearch} />
      <Box flex jus='between' py={2}>
        <Box grow />

        <Link to='add'>
          <Button>+新增</Button>
        </Link>
      </Box>
      <DictionaryTable ref={tableRef} size='middle' serviceData={searchData} sortToData={null} actionWidth={300} />
    </>
  );
}

export default DictionaryPage;
