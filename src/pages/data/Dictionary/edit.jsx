import { useQueryValue } from '@/components';
import React from 'react';
import EditDictionary from './components/EditDictionary';

function Edit() {
  const id = useQueryValue('id');

  return <EditDictionary id={id} type='edit' />;
}

export default Edit;
