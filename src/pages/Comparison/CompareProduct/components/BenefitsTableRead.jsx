import BenefitAPI from '@/api/Benefit';
import { useReq, useTableColumns, useToggle } from '@/components';
import Hover, { HoverStyle } from '@/components/Hover';
import { Button, Table } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import FormulaEditor from './FormulaEditor';

const SummaryDialog = ({ row }) => {
  const { content, formula } = row;
  const [showCode, toggle] = useToggle();

  if (!content) return '--';

  return (
    <Hover>
      <div style={{ position: 'relative', paddingTop: 20, paddingLeft: 8 }}>
        {showCode ? <pre>{content}</pre> : <div dangerouslySetInnerHTML={{ __html: content }} />}
        <HoverStyle hoverStyle={{ opacity: 1 }} style={{ position: 'absolute', top: 0, left: 0, opacity: 0 }}>
          <div>
            <Button size='small' type='link' onClick={toggle}>
              查看{showCode ? '富文本' : 'HTML'}
            </Button>

            <FormulaEditor value={formula} readOnly />
          </div>
        </HoverStyle>
      </div>
    </Hover>
  );
};

function BenefitsTableRead(props) {
  const { productCode } = props;
  const xhr = useReq(BenefitAPI.getTree);
  const [list, setList] = useState([]);
  const listRef = useRef([]);
  listRef.current = list;

  useEffect(() => {
    setList([]);
    if (productCode) xhr.start({ planCode: productCode });
    return xhr.cancel;
  }, [productCode]);

  const data = xhr.result?.data;
  useEffect(() => {
    if (data?.length) {
      let list = data.flatMap((item) =>
        item.children.map((child) => ({
          id: child.id,
          groupCode: item.benefitCode,
          groupName: item.benefitName,
          name: child.benefitName,
          content: child.benefitDesc,
          formula: child.benefitFormula,
        })),
      );
      /*
      list = Array(20)
        .fill(1)
        .flatMap((_, i) =>
          list.map((v) => ({
            id: v.id + `${i}`,
            groupCode: v.benefitCode + `${i}`,
            groupName: v.benefitName + `${i}`,
            name: v.benefitName + `${i}`,
            content: v.benefitDesc + `${i}`,
            formula: null,
          })),
        );*/

      list = list.sort((a, b) => (a.groupCode > b.groupCode ? 1 : a.groupCode === b.groupCode ? 0 : -1));
      setList(list);
    }
  }, [data]);

  const columns = useTableColumns([
    {
      dataIndex: 'groupName',
      title: '利益名称',
      width: 120,
      props(n, row, i) {
        const list = listRef.current;
        const sameGroup = (e) => e.groupCode === row.groupCode;
        const rowSpan = list.findIndex(sameGroup) === i ? list.filter(sameGroup).length : 0;
        return { rowSpan };
      },
    },

    { dataIndex: 'name', title: '利益描述名称', width: 120 },
    {
      dataIndex: 'content',
      title: '利益描述',
      render: (v, row) => <SummaryDialog row={row} />,
      props: {
        style: {
          paddingTop: 0,
          paddingBottom: 0,
        },
      },
    },
  ]);

  return (
    <Table
      pagination={false}
      rowKey='id'
      size='small'
      dataSource={list}
      columns={columns}
      loading={xhr.status === 'loading'}
      scroll={{ y: 300 }}
    />
  );
}

export default BenefitsTableRead;
