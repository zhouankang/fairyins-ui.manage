import { SimpleEditor, useCall, useToggle } from '@/components';
import { wait } from '@/utils';
import { Button, Input } from 'antd';
import { debounce } from 'loadsh/function';
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';

const isEmptyHTML = (html) => {
  const div = document.createElement('div');
  div.innerHTML = html;
  return div.innerText.trim() === '' && div.querySelectorAll('img,video,table').length === 0;
};
const isEmptyString = (string) => {
  return (string ?? '').trim() === '';
};

function DescEditor({ value, onChange, editing: $editing }) {
  const [showCode, toggleShow] = useToggle();
  const [editing, toggleEditing] = useToggle($editing);
  const [content, setContent] = useState(value);
  const setContentChange = useCall((e) => {
    setContent(e.target.value);
  });
  const editorRef = useRef();
  const showCodeRef = useRef();
  showCodeRef.current = showCode;

  useEffect(() => {
    if (value !== content) setContent(value);
  }, [value]);

  const onBlur = useCall(async () => {
    let newContent = isEmptyHTML(content) ? null : content.replace(/[\r\n]/g, '');
    onChange?.(newContent);
    if (newContent == null) setContent(null);
    if ($editing != null) return;
    if (await waitNextShow()) toggleEditing();
  });
  const debounceChange = useRef(debounce(onBlur)).current;
  useEffect(() => {
    if ($editing === true) debounceChange();
  }, [content]);

  const waitNextShow = useCall(async () => {
    const thisShow = showCode;
    await wait(500);
    const nextShow = showCodeRef.current;
    return thisShow === nextShow;
  });

  const html = isEmptyHTML(content) ? '<span class="c-text-secondary">(暂无内容, 点击编辑)</span>' : content;

  const showCodeBtn = (
    <Button size='small' type='link' onClick={toggleShow}>
      {showCode ? '富文本编辑' : 'HTML编辑'}
    </Button>
  );

  useLayoutEffect(() => {
    if (editing && !showCode) {
      editorRef.current?.requestFocus?.();
    }
  }, [editing, showCode]);

  if (editing) {
    return (
      <div key='editing' style={{ position: 'relative', paddingTop: 20, paddingLeft: 8 }}>
        <div style={{ position: 'absolute', top: 0, left: 0 }}>{showCodeBtn}</div>
        {showCode ? (
          <Input.TextArea key='textarea' autoFocus onBlur={onBlur} onChange={setContentChange} defaultValue={content} />
        ) : (
          <SimpleEditor key='editor' ref={editorRef} onBlur={onBlur} onChange={setContent} value={content} />
        )}
      </div>
    );
  }

  return (
    <div
      onClick={toggleEditing}
      style={{ minHeight: 24, cursor: 'pointer' }}
      title='(点击编辑)'
      dangerouslySetInnerHTML={{ __html: html }}
    />
  );
}

export default DescEditor;
