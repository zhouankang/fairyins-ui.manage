import ProductCompareAPI from '@/api/ProductCompare';
import {
  Box,
  Icon,
  mapField,
  PaginationTable,
  SearchFields,
  SelectWithEmpty,
  useCall,
  usePaginationCommon,
} from '@/components';
import { CompanySearchField } from '@/pages/components';
import { forkHandler, pathJoinParams, pick } from '@/utils';
import { Button } from 'antd';
import React, { useRef } from 'react';
import { Link } from 'react-router-dom';

function CompareProductPage(props) {
  const { history } = props;
  const { tableRef, searchData, onSearch, onSelect, selectedRef, reset } = usePaginationCommon();

  const onRemove = useCall(forkHandler((r) => ProductCompareAPI.removeCompareProduct(r.id), reset, true));
  const onToggle = useCall(
    forkHandler(
      (r) =>
        ProductCompareAPI.updateCompareProduct({
          id: r.id,
          compareStatus: r.compareStatus === '1' ? '0' : '1',
        }),
      (r, i) => {
        tableRef.current.setData((prev) => {
          const next = prev.slice();
          next[i] = Object.assign({}, next[i], { compareStatus: r.compareStatus === '1' ? '0' : '1' });
          return next;
        });
      },
      true,
    ),
  );
  const onEdit = useCall((r) =>
    history.push(pathJoinParams('edit', pick(r, ['productCode', 'productName', 'productCategory'])[0])),
  );

  const fields = useRef(
    [
      [['companyName'], '保险公司', CompanySearchField],
      [['productName'], '险种名称'],
      [['productCategory'], '险种分类', 'dict', { component: SelectWithEmpty, code: 'agency', hides: [''] }],
    ].map(mapField),
  ).current;

  const columns = useRef([
    { key: 'index', format: 'order', width: 64 },
    { dataIndex: 'productCode', title: '险种编码', minWidth: 100, ellipsis: true },
    { dataIndex: 'productName', title: '险种名称', minWidth: 160, ellipsis: true },
    { dataIndex: 'companyName', title: '保险公司', width: 110, ellipsis: true },
    { dataIndex: 'productCategory', title: '险种分类', dict: 'agency', width: 96 },
    { dataIndex: 'createTime', title: '创建日期', width: 120 },
    { dataIndex: 'compareStatus', title: '状态', map: { 1: '启用中', 0: '停用中' }, width: 80 },
  ]).current;
  const actions = useRef({
    toggle: (row) => (
      <Button type='link' size='small'>
        {{ 1: '禁用对比', 0: '启用对比' }[row.compareStatus]}
      </Button>
    ),
    remove: {
      confirm: '确认删除，删除后前端将不支持选择此产品',
      text(row) {
        return <Button type='link' size='small' disabled={row.compareStatus === '1'} icon={<Icon type='delete' />} />;
      },
    },
    edit(row) {
      return <Button type='link' size='small' disabled={row.compareStatus === '1'} icon={<Icon type='edit' />} />;
    },
  }).current;

  return (
    <>
      <SearchFields fields={fields} onSubmit={onSearch} onReset={onSearch} />

      <Box flex dir='row' ali='center' jus='between' py={1}>
        <div></div>

        <Link to='edit'>
          <Button>+新增对比产品</Button>
        </Link>
      </Box>

      <PaginationTable
        ref={tableRef}
        columns={columns}
        actions={actions}
        actionCol={{ width: 160 }}
        service={ProductCompareAPI.getCompareProductList}
        serviceData={searchData}
        onToggle={onToggle}
        onRemove={onRemove}
        onEdit={onEdit}
      />
    </>
  );
}

export default CompareProductPage;
