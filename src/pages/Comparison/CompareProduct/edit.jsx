import ProductCompareAPI from '@/api/ProductCompare';
import {
  Box,
  FieldCols,
  FormSection,
  mapField,
  useCall,
  useFieldCtx,
  useFormValue,
  useQueryValues,
  useReq,
  useTableColumns,
} from '@/components';
import { INPUT_KEYS } from '@/constants/componentKeys';
import { SelectProduct } from '@/pages/components';
import { pathJoinParams, pick, uid } from '@/utils';
import { Button, Form, Input, message, Space, Spin, Table } from 'antd';
import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import BenefitsTableRead from './components/BenefitsTableRead';
import DescEditor from './components/DescEditor';
import FormulaEditor from './components/FormulaEditor';

const SelectProductInput = (props) => {
  const productCode = useFormValue(useFieldCtx('form'), ['productCode']);

  const ref = useRef();

  const check = useCall(() => {
    if (!productCode && !ref.current.isOpen()) {
      ref.current.open();
    }
  });
  useLayoutEffect(() => {
    setTimeout(check);
  }, []);

  const [inputProps, modalProps] = pick(props, INPUT_KEYS);

  const el = <Input {...inputProps} readOnly />;
  if (productCode) return el;

  return (
    <SelectProduct
      ref={ref}
      rowKey='productCode'
      selectedKeys={productCode ? [productCode] : []}
      notCompare
      formLazy
      {...modalProps}
    >
      {el}
    </SelectProduct>
  );
};

const BenefitDesc = ({ row, onChange }) => {
  const setDesc = useCall((benefitDesc) => {
    onChange({ ...row, benefitDesc });
  });
  const setFormula = useCall((benefitFormula) => {
    onChange({ ...row, benefitFormula });
  });
  return (
    <>
      <FormulaEditor value={row.benefitFormula} onChange={setFormula} />
      <DescEditor value={row.benefitDesc} onChange={setDesc} />
    </>
  );
};

function EditCompareProductPage(props) {
  const xhr = useReq(ProductCompareAPI.getBenefitsByCode);
  const saveXhr = useReq(ProductCompareAPI.saveBenefits);
  const cateXhr = useReq(ProductCompareAPI.getDictList);
  const [productCode, productName, productCategory] = useQueryValues('productCode', 'productName', 'productCategory');
  const [form] = Form.useForm();

  useEffect(() => {
    if (productCategory) {
      cateXhr.start({ t: { type: '1', productCategory } });
    }
  }, [productCategory]);
  useEffect(() => {
    form.setFieldsValue({ productCode, productName });
  }, [productCode, productName]);

  useEffect(() => {
    if (productCode) xhr.start(productCode);
    return xhr.cancel;
  }, [productCode]);
  const rowSpanRef = useRef();

  const data = xhr.result?.data;
  const cate = cateXhr.result?.data?.records; // 对比项

  const [list, setList] = useState([]);
  const listRef = useRef(list);
  listRef.current = list;
  useEffect(() => {
    if (data && cate) {
      const flatten = cate.flatMap((item) => item.compareCategoryList);
      data.forEach((row) => {
        row.key = row.id ?? uid();
      });

      // 按照对比项查询结果排序
      let list = flatten.flatMap((i) => {
        const dataItem = data.find((ii) => {
          return ii.categoryCode === i.code && ii.parentCategoryCode === i.parentCode;
        });
        if (dataItem) return [dataItem];
        return [];
      });
      setList(list);
      // form.setFieldsValue({ list: data });
      rowSpanRef.current = list.reduce((r, i) => {
        r[i.parentCategoryCode] ??= 0;
        r[i.parentCategoryCode] += 1;
        return r;
      }, {});
    } else {
      setList([]);
    }
  }, [data, cate]);

  const onSelectProduct = useCall(([productItem]) => {
    const param = pick(productItem, ['productCode', 'productName', 'productCategory'])[0];
    props.history.replace(pathJoinParams(param));
  });

  const fields = [
    [
      'productCode',
      '险种编码',
      SelectProductInput,
      { initialValue: productCode, readOnly: true, onOk: onSelectProduct },
    ],
    ['productName', '险种名称', null, { initialValue: productName, readOnly: true }],
  ].map(mapField);

  const renderDesc = useCall((v, row, index) => (
    <BenefitDesc
      row={row}
      onChange={(item) => {
        setList((list) => {
          const next = list.slice();
          next[index] = item;
          return next;
        });
      }}
    />
  ));

  const columns = useTableColumns([
    {
      dataIndex: 'parentCategoryName',
      title: '对比类别',
      width: 120,
      props(val, row, index) {
        const list = listRef.current;
        const code = list[index]['parentCategoryCode'];
        const isFirst = list.findIndex((r) => r.parentCategoryCode === code) === index;
        return { rowSpan: isFirst ? rowSpanRef.current?.[code] ?? 1 : 0 };
      },
    },
    { dataIndex: 'categoryName', title: '对比子类', width: 120 },
    {
      dataIndex: 'benefitDesc',
      title: '对比子类描述',
      render: renderDesc,
      props: {
        style: { paddingTop: 4, paddingBottom: 4 },
      },
    },
  ]);

  const onSave = useCall(async () => {
    const data = list.map((item) => pick(item, ['key'])[1]);
    await saveXhr.start(data, productCode);
    message.success('保存成功');
    props.history?.goBack();
  });

  const loading = [xhr.status, saveXhr.status, cateXhr.status].includes('loading');

  return (
    <Spin spinning={loading}>
      <Form form={form} component={false}>
        <FormSection title='选择险种'>
          <FieldCols form={form} fields={fields} />
        </FormSection>
        {/* <Expandable defaultExpanded>
          <FormSection title='计划书保障责任' right={<Expandable.Trigger />}>
            <Expandable.Content>
              <div></div>
            </Expandable.Content>
          </FormSection>
        </Expandable>*/}

        <BenefitsTableRead productCode={productCode} />

        <FormSection title='保障对比维护'>
          <Table rowKey='key' dataSource={list} columns={columns} pagination={false} />
        </FormSection>
      </Form>

      <Box flex jus='center' py={3} className='form-page-btn'>
        <Space size='large'>
          <Button onClick={props.history?.goBack}>返回</Button>
          <Button type='primary' onClick={onSave}>
            保存
          </Button>
        </Space>
      </Box>
    </Spin>
  );
}

export default EditCompareProductPage;
