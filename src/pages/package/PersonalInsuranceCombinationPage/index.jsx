import PackageAPI from '@/api/Package';
import {
  Box,
  mapField,
  PaginationTable,
  PopoverConfirm,
  SearchFields,
  useCall,
  usePaginationCommon,
} from '@/components';
import { PACKAGE_COLUMNS, PACKAGE_FIELDS } from '@/constants/sharedFields';
import { CompanySelectField, LinkWithSelectCompany } from '@/pages/components';
import { forkHandler } from '@/utils';
import { Button } from 'antd';
import React from 'react';

const fields = [...PACKAGE_FIELDS, mapField([['t', 'companyCode'], '保险公司', CompanySelectField])];

function PersonalInsuranceCombinationPage() {
  const { tableRef, searchData, onSearch, onSelect, selectedRef, hasSelected, reset } = usePaginationCommon();

  const onBatchDel = useCall(forkHandler(() => PackageAPI.remove(selectedRef.current), reset, true));

  return (
    <>
      <SearchFields fields={fields} onSubmit={onSearch} onReset={onSearch} />

      <Box flex jus='between' py={2}>
        {hasSelected && (
          <PopoverConfirm onOk={onBatchDel}>
            <Button>批量删除</Button>
          </PopoverConfirm>
        )}

        <span />

        <LinkWithSelectCompany to='addPage'>
          <Button>+新增</Button>
        </LinkWithSelectCompany>
      </Box>

      <PaginationTable
        ref={tableRef}
        rowKey='packageCode'
        service={PackageAPI.findByPage}
        serviceData={searchData}
        columns={PACKAGE_COLUMNS}
        rowSelection={{
          onChange: onSelect,
        }}
      />
    </>
  );
}

export default PersonalInsuranceCombinationPage;
