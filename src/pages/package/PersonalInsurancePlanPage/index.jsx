import PlanAPI from '@/api/Plan';
import {
  Box,
  mapField,
  PaginationTable,
  PopoverConfirm,
  SearchFields,
  useCall,
  usePaginationCommon,
} from '@/components';
import { CompanySelectField, LinkWithSelectCompany } from '@/pages/components';
import { forkHandler } from '@/utils';
import { Button } from 'antd';
import React from 'react';

const FIELDS = [
  [['searchFuzzy', 'plan_business_code'], '计划业务编码'],
  [['searchFuzzy', 'plan_code'], '计划编码'],
  [['searchFuzzy', 'plan_name'], '计划名称'],
  // [['t', 'saleStateMark'], '计划状态', 'select', { options: enumToOptions(SALES_STATE), allowClear:
  // true }],
  [['t', 'companyCode'], '保险公司', CompanySelectField],
].map(mapField);

const COLUMNS = [
  { key: 'index', title: '序号', format: 'order', width: 64 },
  { dataIndex: 'planBusinessCode', title: '计划业务编码', link: 'addPage', width: 160, sorter: true },
  { dataIndex: 'planCode', title: '计划编码', width: 160, sorter: true },
  { dataIndex: 'companyName', title: '保险公司', width: 110, ellipsis: true },
  { dataIndex: 'planName', title: '计划名称', width: 200 },
  // { dataIndex: 'saleStateMark', title: '计划状态', width: 96, map: SALES_STATE },
  { dataIndex: 'num', title: '险种数', width: 80, align: 'right' },
];

function PersonalInsurancePlanPage() {
  const { tableRef, searchData, onSearch, selectedRef, reset } = usePaginationCommon();

  const onBatchDel = useCall(forkHandler(() => PlanAPI.remove(selectedRef.current), reset, true));

  return (
    <>
      <SearchFields fields={FIELDS} onSubmit={onSearch} onReset={onSearch} />

      <Box flex jus='between' py={2}>
        <PopoverConfirm content='确认删除已选择的计划吗?' onOk={onBatchDel}>
          <span />
          {/*<Button>批量删除</Button>*/}
        </PopoverConfirm>

        <LinkWithSelectCompany to='addPage'>
          <Button>+新增</Button>
        </LinkWithSelectCompany>
      </Box>

      <PaginationTable
        ref={tableRef}
        service={PlanAPI.findByPage}
        serviceData={searchData}
        columns={COLUMNS}
        // rowSelection={{
        //   onChange: onSelect,
        // }}
      />
    </>
  );
}

export default PersonalInsurancePlanPage;
