import BenefitAPI from '@/api/Benefit';
import { Box, FormSection, Icon, useCall, useReq, useSimpleMemo, useTableActions, useTableColumns } from '@/components';
import { forkHandler } from '@/utils';
import { Button, message, Table } from 'antd';
import React, { forwardRef, useEffect, useImperativeHandle } from 'react';
import BenefitDataEditModal from './BenefitDataEditModal';
import { useDefinitionData } from './Context';

const COLUMNS = [
  { title: '利益名称', dataIndex: 'benefitName' },
  { title: '利益代码', dataIndex: 'benefitCode', align: 'center' },
];

const ACTIONS = {
  addChild: {
    dialog: (props) => <BenefitDataEditModal {...props} resetValue={{ isShow: props.parent.isShow }} />,
    type: 0,
    itemProp: 'parent',
    text: (
      <Box component={Button} px={0.5} size='small' type='link'>
        添加下级
      </Box>
    ),
    onOk: (row, index, formData) => BenefitAPI.add(formData),
  },
  addSib: {
    dialog: BenefitDataEditModal,
    type: 0,
    itemProp: 'sibling',
    text: (
      <Box component={Button} px={0.5} size='small' type='link'>
        添加同级
      </Box>
    ),
    onOk: (row, index, formData) => BenefitAPI.add(formData),
  },
  edit: {
    dialog: BenefitDataEditModal,
    type: 0,
    itemProp: 'item',
    text: <Button type='link' icon={<Icon type='edit' />} />,
    onOk: (row, index, formData) => BenefitAPI.add(formData),
  },
  del: {
    confirm: void 0,
    beforeConfirm(row) {
      if (row?.children?.length) {
        message.warn('无法直接删除父节点');
        return false;
      }
    },
    onOk: (row) => BenefitAPI.remove([row.id]),
    text: <Button type='link' icon={<Icon type='delete' />} />,
  },
};

function BenefitDescConfig(props, ref) {
  useImperativeHandle(ref, () => null, []);
  const definitionData = useDefinitionData();
  const { productCode } = definitionData?.productDefineList?.[0] ?? {};
  const xhr = useReq(BenefitAPI.getTree);

  const refresh = useCall(() => xhr.start({ planCode: productCode, type: 0 }));

  useEffect(() => {
    if (productCode) refresh();
  }, [productCode]);

  const columns = useSimpleMemo(
    useTableColumns(COLUMNS).concat(
      useTableActions(
        ACTIONS,
        {
          onAfterAddChild: refresh,
          onAfterAddSib: refresh,
          onAfterEdit: refresh,
          onAfterDel: refresh,
        },
        { align: 'center', width: 256 },
      ),
    ),
  );

  const onAdd = useCall(
    forkHandler(
      (formData) =>
        BenefitAPI.add({
          type: 0,
          layer: 1,
          parentCode: 0,
          planCode: productCode,
          productCode,
          ...formData,
        }),
      refresh,
      true,
    ),
  );

  const add = (
    <BenefitDataEditModal onOk={onAdd} type={0}>
      <Button type='primary' size='middle'>
        +新增配置
      </Button>
    </BenefitDataEditModal>
  );

  return (
    <FormSection title='利益演示表头配置' right={!xhr.result?.data?.length && add}>
      <Table
        pagination={false}
        rowKey='id'
        loading={xhr.status === 'loading'}
        dataSource={xhr.result?.data ?? []}
        columns={columns}
      />
    </FormSection>
  );
}

BenefitDescConfig = forwardRef(BenefitDescConfig);

export default BenefitDescConfig;
