import { FormSection, useTableActions, useTableColumns } from '@/components';
import { pathJoinParams, pick } from '@/utils';
import { Button, Space, Table } from 'antd';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';

function ShowProductList(props) {
  const { onSaleItem } = props;
  let planList = [];
  try {
    planList = JSON.parse(onSaleItem.packageTree).planList || [];
  } catch {}

  const prodList = planList
    .flatMap((v) =>
      (v?.productList || []).map((i) => Object.assign(pick(v, ['planName', 'planCode', 'planBussinessCode'])[0], i)),
    )
    .filter((e, i, a) => {
      return i === a.findIndex((v) => v.salesProductCode === e.salesProductCode);
    });

  const columns = useTableColumns([
    { dataIndex: 'planBussinessCode', title: '计划编码', width: 140 },
    { dataIndex: 'planName', title: '计划名称', minWidth: 100, ellipsis: true },
    { dataIndex: 'productCode', title: '险种编码', width: 140 },
    { dataIndex: 'productName', title: '险种名称', minWidth: 100, ellipsis: true },
    {
      key: 'actions',
      width: 140,
      render(_, row) {
        const { productCode: businessCode, planBussinessCode: planBusinessCode } = row;
        return (
          <Space>
            <Link
              to={
                '../product/BasicProductsPage/addBasicProducts?' + new URLSearchParams({ action: 'edit', businessCode })
              }
            >
              <Button size='small' type='link' style={{ fontSize: 11 }}>
                编辑险种
              </Button>
            </Link>
            <Link
              to={
                '../package/PersonalInsurancePlanPage/addPage?' +
                new URLSearchParams({
                  planBusinessCode,
                })
              }
            >
              <Button size='small' type='link' style={{ fontSize: 11 }}>
                编辑计划
              </Button>
            </Link>
          </Space>
        );
      },
    },
  ]);

  return (
    <FormSection title='险种列表'>
      <Table pagination={false} dataSource={prodList} columns={columns} size='small' />
    </FormSection>
  );
}

export default ShowProductList;
