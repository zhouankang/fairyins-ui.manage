import { createSharedHook, useReq } from '@/components';
import { useMemo } from 'react';

function useOrgDataHook() {
  const xhr = useReq({
    url: '../system/organizationChart/getOrganizationChartList',
    method: 'POST',
  });
  xhr.startOnce();

  return useMemo(() => {
    const data = xhr.result?.data?.data;
    if (!data) return null;

    const each = (item) => {
      let nItem = { value: item.code, label: item.cnName };
      if (item.subOrgList?.length) {
        nItem.children = item.subOrgList.map(each);
      }
      return nItem;
    };
    return [each(data)];
  }, [xhr.result?.data?.data]);
}

const useOrgData = createSharedHook(useOrgDataHook);
export default useOrgData;
