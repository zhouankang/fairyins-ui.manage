import { useQueryValue } from '@/components';
import { UserActions } from '@/reducers/user';
import RouteConfig from '@/RouteConfig';
import { getQueryValue, setToken } from '@/utils';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

function ProductCenter() {
  const token = useQueryValue('token');
  const dispatch = useDispatch();

  useEffect(() => {
    if (token) dispatch(UserActions.setToken(token));
  }, [token]);

  return <RouteConfig parentPath='/product' />;
}

export default ProductCenter;
