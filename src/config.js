import { getQueryValue, on, setToken } from '@/utils';
import { Form, Select } from 'antd';
import 'moment/locale/zh-cn';

Form.defaultProps = {
  ...Form.defaultProps,
  scrollToFirstError: true,
};

Select.defaultProps = {
  ...(Select.defaultProps || null),
  virtual: false,
};

// https://stackoverflow.com/questions/3522090/event-when-window-location-href-changes
(() => {
  const listener = () => {
    let search = location.search;
    let hashSearch = location.hash.split('?')[1];
    search = [search, hashSearch].filter(Boolean).join('&');

    const token = getQueryValue(search, 'token');
    if (token) setToken(token);
  };
  const observer = new MutationObserver(listener);
  observer.observe(document.body, { childList: true, subtree: true });
  on(window, 'hashchange', listener);
  setTimeout(listener);
})();
