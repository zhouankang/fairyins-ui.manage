/**
 * @typedef {Object} Resource           菜单节点
 * @property {String} id
 * @property {String} [parentId]
 * @property {String} resourceName      菜单名称
 * @property {String} resourceType      类型: M 菜单；B 按钮/子页面
 * @property {String} url               路径
 * @property {Boolean} enabled          是否启用
 * @property {String} [icon]            图标
 * @property {String} [iframeUrl]       iframe页面路径
 */

/**
 * @typedef {Array<Resource & { children: ResourceTree }>} ResourceTree     菜单节点树
 */

const flatMap = (data) => data.flatMap(({ childs, children, ...item }) => [item, ...flatMap(childs ?? children ?? [])]);

/**
 *
 * @param {Array<Resource>} data
 * @return {Promise<ResourceTree>}
 */
const toResourceTree = async (data) => {
  const groupByParent = {};
  const safeData = flatMap(JSON.parse(JSON.stringify(data)));
  await Promise.all(
    safeData.map(async (item) => {
      const parentId = item.parentId || 0;
      if (!groupByParent[parentId]) groupByParent[parentId] = [];
      groupByParent[parentId].push(item);
      await Promise.resolve();
      item.children = groupByParent[item.id] || [];
    }),
  );

  return groupByParent[0] || [];
};

export default toResourceTree;
