import { render, unmountComponentAtNode } from 'react-dom';

const voidRender = (node) => {
  const el = document.createElement('span');
  render(node, el);
  return () => unmountComponentAtNode(el);
};

export default voidRender;
