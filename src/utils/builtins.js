export const requestIdleCallback = (window.requestIdleCallback =
  window.requestIdleCallback ||
  function (cb) {
    let start = Date.now();
    return setTimeout(function () {
      cb({
        didTimeout: false,
        timeRemaining: function () {
          return Math.max(0, 50 - (Date.now() - start));
        },
      });
    }, 1);
  });
