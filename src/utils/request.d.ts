import {
  AxiosInstance,
  AxiosInterceptorManager,
  AxiosPromise,
  AxiosRequestConfig as AxiosReqConfig,
  AxiosResponse,
} from 'axios';

export interface AxiosRequestConfig extends AxiosReqConfig {}

export type CODES = {
  [index: string]: string | string[];
};

export type RespCode = '000' | '007' | '-1' | '-2' | string;

export interface ApiResp<T = any> {
  code: RespCode;
  msg: string;
  data?: T;
  response?: AxiosResponse;
}

export interface PaginationReq<T = {}> {
  /** 当前页码 */
  current: number;

  /** 单页条数 */
  pageSize: number;

  /** 模糊查询 */
  searchFuzzy?: Partial<T>;

  /** 搜索范围匹配start */
  sectionStartParams?: Partial<T>;

  /** 搜索范围匹配end */
  sectionEndParams?: Partial<T>;

  /** 排序参数 */
  sortParams?: {
    [index: keyof T]: number;
  };

  /** 匹配查询 */
  t?: Partial<T>;
}

export type PaginationResp<T = {}> = ApiResp<{
  records: T[];

  /** 当前页码 */
  current: number;

  /** 总条数 */
  total: number;

  /** 总页数 */
  pages: number;

  /** 单页条数 */
  size: number;
}>;

export default interface Request extends AxiosInstance {
  defaults: AxiosRequestConfig;
  interceptors: {
    request: AxiosInterceptorManager<AxiosRequestConfig>;
    response: AxiosInterceptorManager<ApiResp>;
  };

  (config: AxiosRequestConfig): AxiosPromise;

  (url: string, config?: AxiosRequestConfig): AxiosPromise;

  getUri(config?: AxiosRequestConfig): string;

  request<T = any, R = ApiResp<T>>(config: AxiosRequestConfig): Promise<R>;

  get<T = any, R = ApiResp<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;

  delete<T = any, R = ApiResp<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;

  head<T = any, R = ApiResp<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;

  options<T = any, R = ApiResp<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;

  post<T = any, R = ApiResp<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>;

  put<T = any, R = ApiResp<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>;

  patch<T = any, R = ApiResp<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>;
}
