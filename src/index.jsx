import '@/wdyr';
import { getQueryValue } from '@/utils';
import { ConfigProvider } from 'antd';
import zhCN from 'antd/es/locale/zh_CN';
import React, { memo } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './index.global.less';
import store from './store';

import('@/config');

let App;
if (process.env.ENTRY === 'product-center' && !getQueryValue(location.search, 'showMenu')) {
  App = require('./ProductCenter').default;
} else {
  App = require('./App').default;
}
App = memo(App, () => true);

if (process.env.NODE_ENV === 'development') {
  const AppComponent = App;
  App = () => (
    <Switch>
      <Route path='/examples' component={require('@/examples').default} />
      <Route>
        <AppComponent />
      </Route>
    </Switch>
  );
}

const root = document.getElementById('root');

render(
  <Provider store={store}>
    <HashRouter>
      <ConfigProvider locale={zhCN}>
        <App hideHead={process.env.NODE_ENV === 'production'} />
      </ConfigProvider>
    </HashRouter>
  </Provider>,
  root,
);
