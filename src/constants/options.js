export const LANGUAGE_KEYS = {
  cn: 'zh_CN', // 中文
  en: 'en_US', // 英文
};

// 语言类型
export const LANGUAGE_TYPE = {
  zh_CN: ('INTL$Language.Chinese', '中文'),
  en_US: ('INTL$Language.English', 'English'),
};

// 险种类型
export const PRODUCT_TYPE_INSURE = {
  0: '主险',
  1: '附加险',
  4: '附加两全',
  2: '投保人豁免险',
  5: '被保人豁免险',
};

// 豁免险类型
export const PRODUCT_TYPES_PB = ['2', '5'];

// 主险标记
export const MAIN_RISK_TYPE = '0';

// 附加险标记集合
export const ADDITION_RISK_TYPE = Object.keys(PRODUCT_TYPE_INSURE)
  .filter((v) => v != MAIN_RISK_TYPE)
  .join(',');

// 责任属性
export const LIABILITY_TYPE = {
  I: ('INTL$ProductCenter.Issued', '承保'),
  P: ('INTL$ProductCenter.Claim', '理赔'),
};

//团个类型
export const PRODUCT_NATURE = {
  P: ('INTL$ProductCenter.PersonalInsurance', '个险'),
  G: ('INTL$ProductCenter.GroupInsurance', '团险'),
};

export const PRODUCT_NATURE_ADD = (messages, key) => {
  if (!messages) return PRODUCT_NATURE;
  const type = {
    P: ('INTL$ProductCenter.PersonalInsurance', '个险'),
    G: ('INTL$ProductCenter.GroupInsurance', '团险'),
  };
  return key ? type[key] : type;
};

//产品状态
export const PRODUCT_STATE = {
  0: ('INTL$ProductCenter.InTheConfiguration', '配置中'),
  1: ('INTL$ProductCenter.Completed', '已完成'),
};

//长短类型
export const TIME_RISK_MARK = {
  // L: '长期（一年期以上或含保证续费）',
  // M: '一年期',
  // S: '极短险（一年期以下）'
  L: ('INTL$ProductCenter.LongTerm', '长期'),
  M: ('INTL$ProductCenter.OneYearPeriod', '一年期'),
  S: ('INTL$ProductCenter.VeryShortRisk', '极短险'),
};
//长短类型
export const TIME_RISK_MARK_ADD = (messages, key) => {
  if (!messages) return TIME_RISK_MARK;
  const type = {
    L: ('INTL$ProductCenter.LongTerm', '长期'),
    M: ('INTL$ProductCenter.OneYearPeriod', '一年期'),
    S: ('INTL$ProductCenter.VeryShortRisk', '极短险'),
  };
  return key ? type[key] : type;
};

//保障期限类型
export const GUARANTEED_TYPE = {
  //0: '时间',
  //1: '年龄'
  0: ('INTL$Public.Time', '时间'),
  1: ('INTL$Public.Age', '年龄'),
};

//保障期限单位
export const GUARANTEED_UNIT = {
  //Y: '年',
  //M: '月',
  //D: '天',
  //A: '岁'
  Y: ('INTL$Public.Year', '年'),
  M: ('INTL$Public.Month', '月'),
  D: ('INTL$Public.Day', '天'),
  A: ('INTL$Public.YearOld', '岁'),
};
//保障期限年龄单位
export const GUARANTEED_AGE_UNIT = {
  //M: '月',
  //D: '天',
  //A: '岁'
  M: ('INTL$Public.Month', '月'),
  D: ('INTL$Public.Day', '天'),
  A: ('INTL$Public.YearOld', '岁'),
};

//保障期限年龄单位
export const GUARANTEED_UNIT_GUAR = {
  //Y: '年',
  //A: '岁'
  Y: ('INTL$Public.Year', '年'),
  A: ('INTL$Public.YearOld', '岁'),
};

//投保年龄单位
export const ALLOW_AGE_UNIT = {
  //D: '天',
  //A: '岁'
  D: ('INTL$Public.Day', '天'),
  A: ('INTL$Public.YearOld', '岁'),
};

// 性别限制
export const REFUSE_GENDER = {
  //'': '无限制',
  //M: '男',
  //F: '女'
  '': ('INTL$ProductCenter.Unlimited', '无限制'),
  M: ('INTL$ProductCenter.Male', '男'),
  F: ('INTL$ProductCenter.Female', '女'),
};

//社保限制
export const REFUSE_SOCIAL_SECURITY = {
  //'': '无限制',
  //1: '有社保',
  //2: '无社保'
  '': ('INTL$ProductCenter.Unlimited', '无限制'),
  1: ('INTL$ProductCenter.SocialSecurity', '有社保'),
  0: ('INTL$ProductCenter.NoSocialSecurity', '无社保'),
};

//社保限制
export const REFUSE_SOCIAL = {
  //"null": '无限制',
  //"Y": '有社保',
  //"N": '无社保'
  // [null]: ('INTL$ProductCenter.Unlimited', '无限制'),
  Y: ('INTL$ProductCenter.SocialSecurity', '有社保'),
  N: ('INTL$ProductCenter.NoSocialSecurity', '无社保'),
};

//期缴方式
export const PAYMENT_TYPE = {
  //'00': '趸交',
  //'01': '年缴',
  //'02': '半年缴',
  //'04': '季缴',
  //'12': '月缴',
  0: ('INTL$ProductCenter.WholesalePayment', '趸缴'),
  12: ('INTL$ProductCenter.AnnualPayment', '年缴'),
  6: ('INTL$ProductCenter.Semi-annualPayment', '半年缴'),
  3: ('INTL$ProductCenter.QuarterlyPayment', '季缴'),
  1: ('INTL$ProductCenter.MonthlyPayment', '月缴'),
};

//保费增加方式
export const PREMIUM_ADD_TYPE = {
  //0: '保费',
  //1: '保单',
  0: ('INTL$ProductCenter.Premium', '保费'),
  1: ('INTL$ProductCenter.InsurancePolicy', '保单'),
};

// 范围类型
export const SCOPE_TYPE = {
  //GELT: '左闭右开',
  //GELE: '全闭合',
  //GTLE: '左开右闭',
  //GTLT: '全开',
  //GT: '大于',
  //GE: '大于等于',
  //LT: '小于',
  //LE: '小于等于',
  //CTS: '数字相等',
  //SCTS: '字符串包含（相等）'
  GELT: '[0,＋∞]',
  GELE: '[0,0]',
  GTLE: '[＋∞,0]',
  GTLT: '[-∞,＋∞]',
  GT: '>',
  GE: '>=',
  LT: '<',
  LE: '<=',
  CTS: '==',
  SCTS: '===',
};

export const LIMIT_CONDITION = {
  //"age": "年龄",
  //"paymentMethod": "缴费方式",
  //"protectionPeriod": "保障期限"
  age: ('INTL$Public.Age', '年龄'),
  paymentMethod: ('INTL$ProductCenter.BillingMode', '缴费方式'),
  protectionPeriod: ('INTL$ProductCenter.BenefitPeriod', '保障期限'),
};

// 产品销售状态
export const PRODUCT_SHOP_STATUS = {
  1: '在售',
  2: '停售',
  3: '配置中',
  4: '完成配置',
};

// 产品销售状态
export const PRODUCT_RECOMMEND_STATUS = {
  // 0: '不推荐',
  // 1: '推荐'
  0: ('INTL$ProductCenter.NotRecommended', '不推荐'),
  1: ('INTL$ProductCenter.Recommend', '推荐'),
};

// 产品上架/下架状态
export const PRODUCT_ONLINE_STATUS = {
  // 1: '下架',
  // 2: '上架'
  1: ('INTL$ProductCenter.Offline', '下架'),
  2: ('INTL$ProductCenter.Online', '上架'),
};

// 产品销售类型
export const PRODUCT_SHOP_TYPES = {
  0: '灵活定制',
  1: '组合套餐',
  2: '个险',
};

// 产品组合类型
export const PRODUCT_GROUP_TYPES = {
  //0.单产品组合 1.产品计划 2.多产品组合 3.团险方案
  0: ('INTL$ProductCenter.SingleProductPortfolio', '计划'),
  // 1: '产品计划',
  2: ('INTL$ProductCenter.MultiProductPortfolio', '组合'),
  // 3: ('INTL$ProductCenter.GroupPlan',"团险方案")
};

// 产品营销权限
export const PRODUCT_MARKETING_PERMISS = {
  // 0: '初级',
  // 1: '中级',
  // 2: '高级'
  0: ('INTL$ProductCenter.Primary', '初级'),
  1: ('INTL$ProductCenter.Intermediate', '中级'),
  2: ('INTL$ProductCenter.Advanced', '高级'),
};

// 方案类型packageCategory
export const PACKAGE_CATEGORY = {
  //0: '灵活方案',
  //1: '固定方案'
  0: ('INTL$ProductCenter.FlexibleSolutions', '灵活方案'),
  1: ('INTL$ProductCenter.FixedScheme', '固定方案'),
};

// 系统名称 富卫/四合一
export const SYSTEM_NAME = {
  0: 'FW',
  1: 'SHY',
}[0];

// 属性池 - 属性类型
export const PROPERTY_TYPE = {
  //'BA': '基本信息',
  //'CA': '承保信息',
  //'SA': '销售信息',
  //'SCA': '理赔信息',
  //'PA': '保全信息',
  //'RA': '再保信息',
  //'SCAL': '理赔责任信息',
  //'CAL': '承保责任信息',
  //'PH': '产品条款信息',
  BA: ('INTL$ProductCenter.BasicInformation', '基本信息'),
  CA: ('INTL$ProductCenter.InsuranceInformation', '承保信息'),
  SA: ('INTL$ProductCenter.SalesInformation', '销售信息'),
  SCA: ('INTL$ProductCenter.ClaimInformation', '理赔信息'),
  PA: ('INTL$ProductCenter.SecurityInformation', '保全信息'),
  RA: ('INTL$ProductCenter.ReinsuranceInformation', '再保信息'),
  SCAL: ('INTL$ProductCenter.BasicInformation', '基本信息'),
  CAL: ('INTL$ProductCenter.BasicInformation', '基本信息'),
  PH: ('INTL$ProductCenter.BasicInformation', '基本信息'),
};

// 属性池 - 属性输入类型
export const PROPERTY_INPUT = {
  TB: '文本框',
  NB: '数字框',
  DB: '下拉框',
};

// 费率文件
export const RATE_FILE = {
  A1: '保费费率',
  A2: '职业加费',
  A3: '健康加费',
  A4: '价值费率',
  A5: '风险保额',
};

export const SALES_STATE = {
  // 0: '待售',
  // 1: '在售',
  // 2: '停售'
  0: ('INTL$ProductCenter.ForSale', '待售'),
  1: ('INTL$ProductCenter.OnSale', '在售'),
  2: ('INTL$ProductCenter.Discontinued', '停售'),
};

export const SALES_ATTR = {
  0: '年金',
  1: '人寿',
  2: '医疗',
  //0: ('INTL$ProductCenter.ForSale',"待售"),
  //1: ('INTL$ProductCenter.OnSale',"在售"),
  //2: ('INTL$ProductCenter.Discontinued',"停售")
};

export const PLAN_TYPE = {
  // SM: "单一险种(主)",
  // SA: "单一险种(附)",
  // PM: "险种计划(主+附)",
  // PA: "险种计划(附+附)",
  SM: ('INTL$ProductCenter.SingleInsurance(main)', '单一险种(主)'),
  SA: ('INTL$ProductCenter.SingleInsurance(attached)', '单一险种(附)'),
  PM: ('INTL$ProductCenter.InsurancePlan(main+attached)', '险种计划(主+附)'),
  PA: ('INTL$ProductCenter.InsurancePlan(attached+attached)', '险种计划(附+附)'),
};
export const DATA_SOURCES = {
  //0: "Table",
  //1: "因子库",
  //2: "公式"
  0: ('INTL$ProductCenter.FactorTable', 'Table'),
  1: ('INTL$ProductCenter.FactorLibrary', '因子库'),
  2: ('INTL$ProductCenter.Formula1', '公式'),
};

export const DATA_SOURCES_A = {
  //0: "Table",
  //1: "因子库",
  //2: "公式"
  // 0: ('INTL$ProductCenter.FactorTable',"Table"),
  0: ('INTL$ProductCenter.FactorLibrary', '因子库'),
  1: ('INTL$ProductCenter.Formula1', '公式'),
};

export const DATA_SOURCES_ADD = (messages, key) => {
  if (!messages) return DATA_SOURCES;
  const type = {
    0: ('INTL$ProductCenter.FactorTable', 'Table'),
    1: ('INTL$ProductCenter.FactorLibrary', '因子库'),
    2: ('INTL$ProductCenter.Formula1', '公式'),
  };
  return key ? type[key] : type;
};
