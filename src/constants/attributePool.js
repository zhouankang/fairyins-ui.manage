export const ATTRIBUTE_TYPES_MAP = {
  input: '文本框',
  // textarea: '文本域',
  radio: '单项勾选',
  checkbox: '多项勾选',
  select: '单选下拉框',
  'multi-select': '多选下拉框',
};
