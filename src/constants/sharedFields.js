import { mapField, SelectWithEmpty } from '@/components';
import { PRODUCT_TYPE_INSURE, TIME_RISK_MARK } from '@/constants/options';
import { CompanySearchField } from '@/pages/components';
import { enumToOptions, initialValueByQuery } from '@/utils';

export const FACTOR_SEARCH_FIELDS = [
  [['searchFuzzy', 'code'], '因子代码'],
  [['searchFuzzy', 'cname'], '因子名称'],
  [['searchFuzzy', 'factor_object'], '因子对象', 'dict', { component: SelectWithEmpty, code: 'factorObject' }],
].map(mapField);

export const FACTOR_COLUMNS = [
  { dataIndex: 'code', title: '因子代码', sorter: true },
  { dataIndex: 'cname', title: '因子名称' },
  { dataIndex: 'javaType', title: '数据类型' },
  { dataIndex: 'factorObject', title: '因子对象', dict: 'factorObject' },
  {
    dataIndex: 'createTime',
    title: '创建日期',
    format: 'date',
    sorter: true,
    defaultSortOrder: 'descend',
  },
];

export const FORMULA_SEARCH_FIELDS = [
  [['searchFuzzy', 'code'], '公式代码'],
  [['searchFuzzy', 'formula'], '公式名称'],
  [['searchFuzzy', 'formula_type'], '公式分类', 'dict', { code: 'formulaType', allowClear: true }],
];

export const FORMULA_COLUMNS = [
  { dataIndex: 'code', title: '公式代码' },
  { dataIndex: 'formulaName', title: '公式名称' },
  { dataIndex: 'formulaType', title: '公式分类', dict: 'formulaType' },
  { dataIndex: 'formula', title: '公式内容' },
  {
    dataIndex: 'createDate',
    title: '创建日期',
    format: 'date',
    sorter: true,
    defaultSortOrder: 'descend',
  },
];

export const PACKAGE_COLUMNS = [
  { dataIndex: 'packageCode', title: '组合编码', link: 'addPage', width: 160 },
  { dataIndex: 'companyName', title: '保险公司', width: 110, ellipsis: true },
  { dataIndex: 'packageName', title: '组合名称' },
];

export const PACKAGE_FIELDS = [
  [['searchFuzzy', 'packageCode'], '组合编码'],
  [['searchFuzzy', 'packageName'], '组合名称'],
  // [['t', 'productNature'], '组合状态', 'select', { options: enumToOptions(SALES_STATE) }],
  // [
  //   ['sectionStartParams', 'sale_begin_time'],
  //   '销售日期',
  //   'date',
  //   { getValueProps: dateToMomentProp, getValueFromEvent: momentToDate('start') },
  // ],
  // [
  //   ['sectionEndParams', 'sale_end_time'],
  //   '停售日期',
  //   'date',
  //   { getValueProps: dateToMomentProp, getValueFromEvent: momentToDate('end') },
  // ],
].map(mapField);

export const companyNameField = [
  'companyName',
  '保险公司',
  null,
  {
    required: true,
    // readOnly: true,
    ...initialValueByQuery('companyName'),
  },
];

export const companyFields = [
  companyNameField,
  [
    'companyCode',
    '保险公司编码',
    null,
    {
      required: true,
      // readOnly: true,
      ...initialValueByQuery('companyCode'),
    },
  ],
];

export const PRODUCT_PAGE_FIELDS = [
  // [['searchFuzzy', 'business_code'], '业务编码'],
  [['searchFuzzy', 'product_code'], '险种编码'],
  [['searchFuzzy', 'product_name'], '险种名称'],
  [['t', 'timeRiskMark'], '长短险类型', SelectWithEmpty, { options: enumToOptions(TIME_RISK_MARK) }],
  [['t', 'productCategory'], '险种分类', 'dict', { component: SelectWithEmpty, code: 'agency', hides: [''] }],
  [['t', ''], '主附险类型', SelectWithEmpty, { options: enumToOptions(PRODUCT_TYPE_INSURE) }],
  [['t', ''], '险种是否更新', SelectWithEmpty, { options: enumToOptions(PRODUCT_TYPE_INSURE) }],
].map(mapField);
