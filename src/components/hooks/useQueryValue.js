import { getQueryValue, getQueryValues } from '@/utils';
import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

/**
 * 获取当前react路由的url参数
 * @param {String} key
 * @return {string}
 */
function useQueryValue(key) {
  const location = useLocation();
  return getQueryValue(location.search, key);
}

export function useQueryValues(...keys) {
  const location = useLocation();

  return useMemo(() => getQueryValues(location.search, ...keys), [location.search]);
}

export default useQueryValue;
