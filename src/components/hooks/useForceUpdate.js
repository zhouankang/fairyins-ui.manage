import { useReducer } from 'react';

function useForceUpdate() {
  const [_, forceUpdate] = useReducer((v) => v + 1, 0);
  return forceUpdate;
}

export default useForceUpdate;
