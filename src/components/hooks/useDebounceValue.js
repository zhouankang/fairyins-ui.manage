import { useEffect, useState } from 'react';
import { useSetTimeout } from '.';

function useDebounceValue(value, time = 200) {
  const setTimeout = useSetTimeout();
  const [state, setState] = useState(value);

  useEffect(() => {
    return setTimeout(setState, time, value);
  }, [value]);

  return state;
}

export default useDebounceValue;
