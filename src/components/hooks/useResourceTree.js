import toResourceTree from '@/utils/toResourceTree';
import { useEffect, useState } from 'react';

/**
 * @param {Resource[]} data
 * @return {ResourceTree}
 */
function useResourceTree(data) {
  const [tree, update] = useState([]);
  useEffect(() => {
    toResourceTree(data).then(update);
  }, [data]);
  return tree;
}

export default useResourceTree;
