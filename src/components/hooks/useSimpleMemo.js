import { isEqualStrictDeep } from '@/utils';
import { useEffect, useState } from 'react';

/**
 * @template T
 * @param {T} value
 * @param compare
 * @return {T}
 */
function useSimpleMemo(value, compare = isEqualStrictDeep) {
  const [memoValue, update] = useState(value);

  useEffect(() => {
    if (!compare(memoValue, value)) {
      update(value);
    }
  }, [value]);

  return memoValue;
}

export default useSimpleMemo;
