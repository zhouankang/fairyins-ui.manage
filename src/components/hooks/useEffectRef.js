import { useRef } from 'react';

/**
 * 一般我们维护一个回调函数作为子组件参数的时候都会被更新
 * 当需要回调函数不因为重新渲染而更新的时候会使用 useCallback(fn, []);
 * 但是这样声明回调函数无法获取一些状态的最新值 (例如props,state,reducerValue或一些钩子的返回值)
 *
 * 当一个对象只需要(ONLY)在回调函数中使用时，意味着组件并不需要依据这个对象而重新渲染
 * 所以这个对象并不需要用state进行管理，使用ref管理可减少不必要的重新渲染
 * 并且回调函数中可以通过ref.current获得最新值，即便是useCallback优化的函数
 */

/**
 * 这只是一个简单的把对象更新到ref中的hook
 * @param {any} value
 * @return {React.MutableRefObject<any>}
 */
function useEffectRef(value) {
  const ref = useRef(value);
  ref.current = value;

  return ref;
}

export default useEffectRef;
