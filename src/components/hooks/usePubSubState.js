import { useDebugValue, useState } from 'react';
import { useWillMountEffect } from '.';

function usePubSubState(pubsub) {
  const [state, setState] = useState(pubsub?.value ?? void 0);
  useDebugValue(state);
  useWillMountEffect(pubsub?.sub, setState);
  return state;
}

export default usePubSubState;
