import { useCall } from '@/components';
import { useEffect, useRef } from 'react';

function useNextTick() {
  const que = useRef([]).current;
  const nextTick = useCall((fn) => {
    que.push(fn);
  });

  useEffect(() => {
    while (que.length) que.shift()();
  });
  return nextTick;
}

export default useNextTick;
