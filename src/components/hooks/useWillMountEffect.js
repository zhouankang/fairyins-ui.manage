import { useEffect, useRef } from 'react';
import useMountedRef from './useMountedRef';

/**
 * componentWillMount
 * 不要在函数中使用setState或dispatch更新组件
 */
function useWillMountEffect(fn, ...args) {
  const mountRef = useMountedRef();
  const retRef = useRef();

  if (!mountRef.current) {
    retRef.current = fn?.(...args);
  }

  useEffect(() => {
    if (typeof retRef.current === 'function') return retRef.current;
  }, []);
}

export default useWillMountEffect;
