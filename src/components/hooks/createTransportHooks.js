import PubSub from '@/utils/PubSub';
import { useEffect, useState } from 'react';
import useWillMountEffect from './useWillMountEffect';

/**
 * 创建两个钩子，可在节点树中传递值
 * 第一个钩子提供一个对象，同一时间只允许使用一次
 * 第二个钩子可获得这个对象，可同时在多个组件中使用
 * @return {[function(value: any), function(): any]}
 */
function createTransportHooks() {
  const pubsub = new PubSub();

  const useProvide = (providerValue) => {
    useWillMountEffect(pubsub.update, providerValue);
    useEffect(() => {
      pubsub.update(providerValue);
    }, [providerValue]);
  };

  const useValue = () => {
    const [state, setState] = useState(pubsub.value);

    useWillMountEffect(pubsub.sub, (value) => {
      setState(value);
    });

    return state;
  };

  return [useProvide, useValue];
}

export default createTransportHooks;
