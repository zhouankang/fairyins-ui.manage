import { isRef } from '@/utils';

export const doRef = (instance, ref) => {
  if (typeof ref === 'function') {
    ref(instance);
  }
  if (isRef(ref)) {
    ref.current = instance;
  }
};

function useForkRef(refA, refB) {
  return (instance) => {
    doRef(instance, refA);
    doRef(instance, refB);
  };
}

export default useForkRef;
