import { useEffect, useRef, useState } from 'react';

/**
 * 对象的更新前的值
 * @param {any} watch
 */
function usePrevious(watch) {
  const ref = useRef();
  const [value, setState] = useState(null);

  useEffect(() => {
    setState(ref.current);
    ref.current = watch;
  }, [watch]);

  return value;
}

export default usePrevious;
