import { pathJoin } from '@/utils';
import { useMemo } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useSimpleMemo } from '.';

/**
 * @param {String} prefix
 * @return {String[]}
 */
function useSplitPaths(prefix = '/') {
  const path = useMemo(() => pathJoin(prefix, '/:first?/:second?/:third?/:four?'), [prefix]);
  const { first, second, third, four } = useRouteMatch(path)?.params ?? {};
  return useSimpleMemo([first, second, third, four].filter(Boolean));
}

export default useSplitPaths;
