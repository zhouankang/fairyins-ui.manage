import { useEffect, useRef } from 'react';

function useMountedRef() {
  const ref = useRef(false);

  useEffect(() => {
    ref.current = true;
    return () => {
      ref.current = false;
    };
  }, []);

  return ref;
}

export default useMountedRef;
