import { defaultShouldUpdate } from '@/utils';
import { useEffect, useState } from 'react';
import { useSimpleMemo } from '.';

function useEffectState(value, shouldUpdate = defaultShouldUpdate) {
  const [state, update] = useState(value);
  useEffect(() => {
    if (typeof value !== 'undefined' && shouldUpdate(state, value)) update(value);
  }, [value]);

  return useSimpleMemo([state, update]);
}

export default useEffectState;
