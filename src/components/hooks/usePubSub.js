import { defaultShouldUpdate } from '@/utils';
import PubSub from '@/utils/PubSub';
import { useDebugValue, useMemo } from 'react';
import { useWillMountEffect } from '.';

function usePubSub(shouldUpdate = defaultShouldUpdate, initialValue = null) {
  const pubsub = useMemo(() => new PubSub(shouldUpdate), []);
  useDebugValue(pubsub.value);
  useWillMountEffect(() => {
    pubsub.value = initialValue;
  });
  return pubsub;
}

export default usePubSub;
