import { useEffect, useRef } from 'react';
import { useCall } from '.';

function useMounted() {
  const mountedRef = useRef(false);

  useEffect(() => {
    mountedRef.current = true;
    return () => {
      mountedRef.current = false;
    };
  }, []);

  return useCall(() => mountedRef.current);
}

export default useMounted;
