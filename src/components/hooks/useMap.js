import { useState } from 'react';
import { useCall } from '.';

function useMap(initial = []) {
  const [state, setState] = useState(new Map(initial));

  const setMap = useCall((key, value) => {
    setState((prev) => {
      if (key != null) return new Map(prev).set(key, value);
      return new Map();
    });
  });

  return [state, setMap];
}

export default useMap;
