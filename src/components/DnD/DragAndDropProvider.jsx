import { useSimpleMemo } from '@/components';
import PropTypes from 'prop-types';
import React from 'react';
import { ConfigCtx } from './shared';

DragAndDropProvider.propTypes = {
  rowKey: PropTypes.string,
  itemComponent: PropTypes.elementType,
  itemClassName: PropTypes.string,
  itemDraggingClassName: PropTypes.string,
  containerComponent: PropTypes.elementType,
  containerClassName: PropTypes.string,
  containerDraggingClassName: PropTypes.string,
  droppableAreaClassName: PropTypes.string,
  overlayClassName: PropTypes.string,
  getDroppableKeys: PropTypes.func,
  onDrop: PropTypes.func.isRequired,
  canDropOutside: PropTypes.bool,
};

function DragAndDropProvider({ children, ...config }) {
  return <ConfigCtx.Provider value={useSimpleMemo(config)} children={children} />;
}

export default DragAndDropProvider;
