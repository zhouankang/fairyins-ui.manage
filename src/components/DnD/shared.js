import PropTypes from 'prop-types';
import React, { createContext, useContext, useEffect, useState } from 'react';

export const Context = createContext({});

export const RowCtx = createContext({ row: null, index: null, ref: null });

export const ConfigCtx = createContext({
  rowKey: 'id',
  itemComponent: null,
  itemClassName: null,
  itemDraggingClassName: null,
  containerComponent: null,
  containerClassName: null,
  containerDraggingClassName: null,
  droppableAreaClassName: null,
  overlayClassName: null,
  getDroppableKeys: null,
  onDrop: null,
  canDropOutside: PropTypes.bool,
});

export function useDraggingCtx() {
  const ctx = useContext(Context);
  const [draggingCtx, update] = useState(null);
  useEffect(() => {
    return ctx?.subscribe?.(update);
  }, [ctx]);
  return draggingCtx;
}
