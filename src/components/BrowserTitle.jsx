import * as PropTypes from 'prop-types';
import React, { useEffect } from 'react';

// html原始的title值，作为后缀
const appTitle = document.title;

/**
 * 更新document.title
 */
function BrowserTitle({ title }) {
  useEffect(() => {
    if (!title) return;
    const browserTitle = document.title;
    document.title = `${title} - ${appTitle}`;
    return () => {
      document.title = browserTitle;
    };
  }, [title]);

  return null;
}

BrowserTitle.propTypes = {
  /** title */
  title: PropTypes.string,
};

export default BrowserTitle;
