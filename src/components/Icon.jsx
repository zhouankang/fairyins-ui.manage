import * as PropTypes from 'prop-types';
import React, { forwardRef, lazy, Suspense, useMemo } from 'react';

/**
 * @param {String} iconType
 * @return {Promise<{default: React.ElementType}>}
 */
let iconContext = (iconType) => Promise.resolve({ default: () => null });

// 分析资源的时候不解析icons
if (!process.env.ANALYZE) {
  // 所有的icons都会在异步chunks中打包
  const context = require.context('@ant-design/icons', false, /\.jsx?$/, 'lazy');
  const maps = context.keys().reduce((ret, key) => ({ ...ret, [key.match(/[a-z]+/i)[0]]: key }), {});
  iconContext = (iconType) =>
    maps.hasOwnProperty(iconType) ? context(maps[iconType]) : Promise.resolve({ default: () => null });
}

/**
 * 支持懒加载的Icon, 支持v3的type属性
 */
function Icon(iconProps, ref) {
  const { type, ...props } = iconProps;

  const iconType = useMemo(() => {
    let _iconType = type;
    _iconType = _iconType.replace(/-[a-z]/g, (n) => n.substr(1).toUpperCase());
    _iconType = _iconType[0].toUpperCase() + _iconType.substr(1);
    if (['Outlined', 'Filled', 'TwoTone'].every((ends) => !_iconType.endsWith(ends))) {
      _iconType = _iconType + 'Outlined';
    }
    return _iconType;
  }, [type]);

  const Component = useMemo(() => lazy(() => iconContext(iconType)), [iconType]);
  return (
    <Suspense fallback={null}>
      <Component ref={ref} {...props} />
    </Suspense>
  );
}

Icon = forwardRef(Icon);

Icon.propTypes = {
  /** icon名称 */
  type: PropTypes.string.isRequired,
};

export default Icon;
