import React from 'react';
import { Prompt } from 'react-router-dom';

const $fallback = () => false;

function MyPrompt(props) {
  const { when, message, useWhen = $fallback } = props;
  let hookValue = useWhen();
  return <Prompt when={when || hookValue} message={message} />;
}

export default MyPrompt;
