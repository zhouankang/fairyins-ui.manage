import { useCall } from '@/components';
import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react';

function Rerender(props, ref) {
  const [state, setState] = useState(0);
  useEffect(() => {
    if (state !== 0) setState(0);
  }, [state]);
  const rerender = useCall(setState, 1);
  useImperativeHandle(ref, () => ({ rerender }), []);

  return state ? null : props.children;
}

Rerender = forwardRef(Rerender);

export default Rerender;
