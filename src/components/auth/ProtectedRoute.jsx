import { isDispatchLogout } from '@/reducers/user';
import { nextTick, pathJoin } from '@/utils';
import { message } from 'antd';
import React from 'react';
import { Redirect, Route, useLocation } from 'react-router-dom';
import useToken from './useToken';

const ProtectedRoute = (props) => {
  const location = useLocation();
  const isLogin = useToken();

  if (!isLogin) {
    if (!isDispatchLogout()) nextTick(message.warn, '请先登录!');
    return <Redirect from={props.path} to={pathJoin('/login', { from: location.pathname + location.search })} />;
  }

  return <Route {...props} />;
};

export default ProtectedRoute;
