import { useSelector } from 'react-redux';

const useToken = () => {
  return useSelector((state) => state.user.token);
};

export default useToken;
