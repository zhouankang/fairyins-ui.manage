import { useSelector } from 'react-redux';

const selectUser = (state) => state.user.userInfo || null;

function useLoginUser() {
  return useSelector(selectUser);
}

export default useLoginUser;
