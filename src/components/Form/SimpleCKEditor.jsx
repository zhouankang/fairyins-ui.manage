import { useCall } from '@/components';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import React from 'react';

function SimpleCkEditor(props) {
  const { value, onChange, type, ...rest } = props;
  const handleChange = useCall((event, editor) => {
    onChange?.(editor.getData());
  });

  return <CKEditor editor={ClassicEditor} data={value} onChange={handleChange} {...rest} />;
}

export default SimpleCkEditor;
