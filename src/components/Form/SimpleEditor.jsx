import { RichText } from '@/components';
import React, { forwardRef } from 'react';
import SimpleBraftEditor from './SimpleBraftEditor';

function SimpleEditor(props, ref) {
  if (props.readOnly) return <RichText ref={ref} {...props} />;

  return <SimpleBraftEditor ref={ref} {...props} />;
}

SimpleEditor = forwardRef(SimpleEditor);

export default SimpleEditor;
