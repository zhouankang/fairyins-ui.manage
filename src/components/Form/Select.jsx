import { useSimpleMemo } from '@/components';
import { notNull } from '@/utils';
import { Select } from 'antd';
import React from 'react';

export function SelectWithEmpty(props) {
  const {
    valueEmpty = null,
    labelEmpty = '全部',
    empty = { value: valueEmpty, label: labelEmpty },
    value,
    ...rest
  } = props;
  const options = useWithinEmptyOptions(props.options, {
    valueEmpty,
    labelEmpty,
    empty,
  });

  rest.allowClear ??= value != empty.value;

  return <Select {...rest} value={value ?? empty.value} options={options} />;
}

export function useWithinEmptyOptions(
  options = [],
  { valueEmpty = null, labelEmpty = '全部', empty = { value: valueEmpty, label: labelEmpty } } = {},
) {
  return useSimpleMemo([empty, ...options]);
}
