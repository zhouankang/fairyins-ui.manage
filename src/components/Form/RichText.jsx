import { Box } from '@/components';
import React, { forwardRef } from 'react';

function RichText({ value, textProps = null }, ref) {
  return !value?.trim() ? (
    <span ref={ref} className='c-text-secondary'>
      暂无数据
    </span>
  ) : (
    <Box
      ref={ref}
      px={1}
      py={1}
      {...textProps}
      style={{
        border: '1px solid #ccc',
        borderRadius: 4,
        maxHeight: 300,
        overflow: 'auto',
        ...textProps?.style,
      }}
    >
      <div dangerouslySetInnerHTML={{ __html: value }} />
    </Box>
  );
}

RichText = forwardRef(RichText);

export default RichText;
