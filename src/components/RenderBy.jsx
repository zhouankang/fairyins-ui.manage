import { useSetTimeout } from '@/components/hooks';
import { uid } from '@/utils';
import React, { useLayoutEffect, useState } from 'react';

function RenderBy({ render = () => rest.children, by, ...rest }) {
  const [show, set] = useState(null);
  const setTimeout = useSetTimeout();
  useLayoutEffect(() => {
    return setTimeout(set, 0, <React.Fragment key={uid()}>{render(rest)}</React.Fragment>);
  }, by);

  return show;
}

export default RenderBy;
