import { useBreadcrumb, useSplitPaths } from '@/components';
import { pathJoin } from '@/utils';
import { Breadcrumb, Menu } from 'antd';
import React, { forwardRef, useMemo } from 'react';
import { NavLink } from 'react-router-dom';
import Icon from '../Icon';
import styles from './index.less';
import RenderMenu from './RenderMenu';

function AppLayout(
  { children, hideNav = false, hideHead = false, menuTree = [], dropdownMenu, logo, parentPath = '/' },
  ref,
) {
  const defaultSelected = useSplitPaths(parentPath);
  const [first] = defaultSelected;
  const sideMenu = useMemo(() => {
    if (hideNav) return menuTree;
    let list = [];
    if (first) {
      list = menuTree.find((item) => item.url === first)?.children ?? [];
    }
    return list;
  }, [hideNav, menuTree, first]);

  const breadcrumb = useBreadcrumb(sideMenu, { prefix: pathJoin(parentPath, first) });

  return (
    <div className={styles.root} ref={ref}>
      {!hideHead && (
        <div className={styles.title}>
          <div className={styles.container}>
            {logo && <img src={logo} alt='' className={styles.logo} />}
            <h3 className={styles.name}>产品中心</h3>
            <div className={styles.user}>{dropdownMenu}</div>
          </div>

          {!hideNav && (
            <div className={styles.nav}>
              {menuTree.map(
                ({ resourceName, resourceType, enabled = true, url }) =>
                  resourceType === 'M' &&
                  enabled && (
                    <NavLink
                      key={url}
                      className={styles.item}
                      activeClassName={styles.active}
                      exact={!url}
                      to={pathJoin(parentPath, url)}
                    >
                      {resourceName}
                    </NavLink>
                  ),
              )}
            </div>
          )}
        </div>
      )}
      <div className={styles.horizontal}>
        {sideMenu?.length > 0 && (
          <div className={styles.menu}>
            <Menu mode='inline' defaultOpenKeys={defaultSelected} selectedKeys={defaultSelected}>
              {sideMenu.map((item) => (
                <RenderMenu key={item.url} item={item} parentUrl={pathJoin(parentPath, !hideNav ? first : '')} />
              ))}
            </Menu>
          </div>
        )}
        <div className={styles.main}>
          {sideMenu?.length > 0 && (
            <Breadcrumb className={styles.breadcrumb}>
              <Breadcrumb.Item href={'#' + parentPath}>
                <Icon type='home' />
              </Breadcrumb.Item>
              {breadcrumb.map((item, index, arr) => (
                <Breadcrumb.Item
                  key={item.id}
                  href={'#' + pathJoin(parentPath, ...arr.slice(0, index + 1).map((v) => v.url))}
                >
                  {item.resourceName}
                </Breadcrumb.Item>
              ))}
            </Breadcrumb>
          )}
          <div className={styles.mainContent}>{children}</div>
        </div>
      </div>
    </div>
  );
}

AppLayout = forwardRef(AppLayout);

export default AppLayout;
