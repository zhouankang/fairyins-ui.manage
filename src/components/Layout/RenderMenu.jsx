import { Icon } from '@/components';
import { pathJoin } from '@/utils';
import { Menu } from 'antd';
import clsx from 'clsx';
import * as PropTypes from 'prop-types';
import React, { memo, useCallback, useMemo } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';

function RenderMenu(props) {
  let {
    item,
    parentUrl = '',
    itemClassName,
    itemProps = null,
    subMenuClassName,
    subMenuProps = null,
    menuLevel = 1,
    ...rest
  } = props;
  const history = useHistory();
  const { resourceName, resourceType, enabled = true, icon, url, children = [] } = item;
  const currentUrl = useMemo(() => pathJoin(parentUrl, url), [parentUrl, url]);
  const matched = useRouteMatch(currentUrl);
  const iconEl = useMemo(() => (icon || menuLevel === 1 ? <Icon type={icon || 'setting'} /> : null), [icon, menuLevel]);
  const onClick = useCallback(() => {
    if (!matched || !matched.isExact) history.push(currentUrl);
  }, [matched, currentUrl]);

  if (resourceType !== 'M' || !enabled) return null;

  if (children && children.filter((v) => v.resourceType === 'M').length) {
    return (
      <Menu.SubMenu
        key={url}
        className={clsx(subMenuClassName?.className, menuLevel > 1 ? subMenuClassName : itemClassName)}
        data-url={currentUrl}
        title={resourceName}
        icon={iconEl}
        {...rest}
        {...subMenuProps}
      >
        {children.map((child) => (
          <RenderMenu
            key={child.url}
            parentUrl={currentUrl}
            item={child}
            itemClassName={itemClassName}
            itemProps={itemProps}
            subMenuClassName={subMenuClassName}
            subMenuProps={subMenuProps}
            menuLevel={menuLevel + 1}
          />
        ))}
      </Menu.SubMenu>
    );
  }

  return (
    <Menu.Item
      key={url}
      className={clsx(itemProps?.className, menuLevel > 1 ? subMenuClassName : itemClassName)}
      data-url={currentUrl}
      icon={iconEl}
      {...rest}
      {...itemProps}
      onClick={onClick}
    >
      {resourceName}
    </Menu.Item>
  );
}

RenderMenu = memo(RenderMenu);

RenderMenu.propTypes = {
  item: PropTypes.object,
  parentUrl: PropTypes.string,
  itemClassName: PropTypes.string,
  itemProps: PropTypes.object,
  subMenuClassName: PropTypes.string,
  subMenuProps: PropTypes.object,
  menuLevel: PropTypes.number,
};

export default RenderMenu;
