import { Box } from '@/components';
import { Space } from 'antd';
import clsx from 'clsx';
import React from 'react';

function FooterBtn(props) {
  return (
    <Box
      component={Space}
      size='large'
      flex
      jus='center'
      py={3}
      {...props}
      className={clsx('form-page-btn', props.className)}
    />
  );
}

export default FooterBtn;
