const INITIAL_STATE = {
  lang: sessionStorage.getItem('language'),
  menuData: [],
};

const APP_SET_LANG = Symbol('app.setLang');
const INIT_MENU = Symbol('app.initMenu');

const appReducer = (state = INITIAL_STATE, { type, ...action }) => {
  switch (type) {
    case 'APP_RESET':
      return {
        ...state,
        menuData: [],
      };

    case APP_SET_LANG:
      return {
        ...state,
        lang: action.lang,
      };

    case INIT_MENU:
      return {
        ...state,
        menuData: action.menuData,
      };

    default:
      return state;
  }
};

const setLang = (lang) => ({ type: APP_SET_LANG, lang });

export const selectMenuData = (state) => state.app.menuData;

export const AppActions = {
  setLang,
};
export default appReducer;
