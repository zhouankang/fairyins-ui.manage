import { Box, useForceUpdate } from '@/components';
import React, { useEffect } from 'react';
import { Switch } from 'react-router-dom';

let context;
let routes;
let forceUpdate;
getRoutes();
if (module.hot) {
  module.hot.accept(context.id, getRoutes);
}

function getRoutes() {
  context = require.context('./', true, /\.jsx$/, 'lazy');
  routes = context.keys().map((pathname) => {});

  forceUpdate?.();
}

function Examples() {
  const update = useForceUpdate();
  useEffect(() => {
    forceUpdate = update;
    return () => {
      forceUpdate = null;
    };
  }, []);

  return (
    <Box flex dir='row' ali='stretch' scroll>
      <Box style={{ flexBasis: 200 }}></Box>
      <Box grow shrink>
        <Switch></Switch>
      </Box>
    </Box>
  );
}

export default Examples;
