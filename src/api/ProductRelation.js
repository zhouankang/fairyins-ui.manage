import request from '@/utils/request';

/**
 * @typedef {Object} ProductRelation
 * @property {String} id
 * @property {String} businessCode
 * @property {String} productCode
 * @property {String} productName
 * @property {String} productType
 * @property {String} productState
 * @property {String} productNature
 * @property {String} productCategory
 * @property {String} createTime
 * @property {String} companyCode
 * @property {String} companyName
 * @property {String} createUser
 * @property {String} isAnnuityValue
 * @property {String} isCashValue
 * @property {String} isDividend
 * @property {String} isExemptions
 * @property {String} isOneYear
 * @property {String} isPayable
 * @property {String} isSurvivalBonus
 * @property {String} questCode
 * @property {String} renewableAge
 * @property {String} renewableTerm
 * @property {String} riskDiscount
 * @property {String} saleBeginTime
 * @property {String} saleChannel
 * @property {String} saleEndTime
 * @property {String} saleStateMark
 * @property {String} timeRiskMark
 * @property {String} updateTime
 * @property {String} updateUser
 * @property {String} versionNumber
 *
 * @property {Number} num0
 * @property {Number} num1
 * @property {Number} num2
 * @property {Number} num3
 *
 * @property {ProductAdditionalRelation[]} ProductAdditionalRelation
 */

/**
 * @typedef {Object} ProductTreeRelation      关系节点
 * @property {String} id
 * @property {String} parentId
 * @property {String} layer
 * @property {String} businessCode
 * @property {String} productCode
 * @property {String} productName
 * @property {ProductTreeRelation[]} child
 */

/**
 * @typedef {Object} ProductAllRelation             附加/绑定/互斥
 * @property {ProductTreeRelation} productBinding   绑定关系节点
 * @property {ProductTreeRelation[]} productMutexs  互斥关系节点
 * @property {ProductTreeRelation} productOptional  附加关系节点
 */

/**
 * @property {Object} ProductAdditionalRelation
 * @property {String} id: "1251402688524976129"
 * @property {String} parentCode: "PLB000001"
 * @property {String} planBusinessCode: null
 * @property {String} relation: "0"
 * @property {String} relationCode: "B000001"
 * @property {String} relationId: "1251402688524976128"
 * @property {String} relationMark: null
 * @property {String} relationName: "附加险B"
 * @property {String} roleMark: "1"
 * @property {String} salesProductCode: "B000001"
 */

/** 产品关联关系 */
const ProductRelationAPI = {
  /**
   * @param {PaginationReq<ProductRelation>}
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<ProductRelation>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/productadditionalrelationgroup/getproductlist', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   * @param {String} productCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<ProductRelation>>}
   */
  getByCode(productCode, options = null) {
    return request.post(
      '/productadditionalrelationgroup/getProductAndRelation',
      {
        businessCode: productCode,
        relationType: 'S',
        relationMark: '1',
      },
      options,
    );
  },

  /**
   *
   * @param {String} businessCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<ProductAdditionalRelation[]>>}
   */
  getRelationByCode(businessCode, options = null) {
    return request.post('/productadditionalrelationgroup/getRelationByPro', { businessCode }, options);
  },

  /**
   *
   * @param data
   * @param options
   * @return {*}
   */
  insertOrUpdate(data, options = null) {
    return request.post('/productadditionalrelationgroup/addrelation', [data], options);
  },

  /**
   *
   * @param {ProductAllRelation} bindAndMutex
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  saveAllRelation(bindAndMutex, options = null) {
    return request.post('/product/relation/allRelationSaveOrUpdate', bindAndMutex, options);
  },

  /**
   *
   * @param {String} businessCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<ProductAllRelation>>}
   */
  getAllRelation(businessCode, options = null) {
    return request.get('/product/relation/getAllRelationInfo', {
      hideMsg: true,
      dontThrow: true,
      ...options,
      params: { businessCode },
    });
  },

  /**
   *
   * @param {String} businessCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  getOptionalRelation(businessCode, options = null) {
    return request.get('/product/relation/getOptionalInfo', {
      hideMsg: true,
      dontThrow: true,
      ...options,
      params: { businessCode },
    });
  },
  /**
   *
   * @param {ProductTreeRelation} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  saveOptionalRelation(data, options = null) {
    return request.post('/product/relation/optionalSaveOrUpdate', data, options);
  },
};

export default ProductRelationAPI;
