import request from '@/utils/request';

/**
 * @typedef {Object} Liability                责任
 * @property {String} id
 * @property {String} liabilityType           责任属性 枚举 LIABILITY_TYPE
 * @property {String} liabilityDesc           责任描述
 * @property {String} liabilityName           责任名称
 * @property {String} liabilityCode           责任代码
 * @property {String} liabilityCategory
 * @property {String} liabilityProductType
 * @property {String} liabilityFormula
 * @property {Date} createDate
 */

/** LiabilityApi : 责任管理 */
const LiabilityAPI = {
  /**
   * 责任列表
   * @param {PaginationReq<Liability>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<Liability>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/liability/liabilityFindPage', { current, pageSize, ...reqData }, options);
  },

  /**
   * @param {Liability.id} id
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<Liability>>}
   */
  getItemInfo(id, options = null) {
    return request.post('/liability/seleobj', null, { ...options, params: { id } });
  },

  /**
   * @param {Liability} item
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  updateInsert(item, options = null) {
    return request.post('/liability/insertorupdate', item, options);
  },

  remove(idList, options = null) {
    return request.post('/liability/deleobj', null, {
      ...options,
      params: { ids: idList.join(','), type: 0 },
    });
  },
};

export default LiabilityAPI;
