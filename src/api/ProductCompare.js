import request from '@/utils/request';

/**
 * @typedef {Object} CompareCategory             对比责任字典
 * @property {String} id                         "string",
 * @property {String} level                      "string", 层级
 * @property {String} sort                       "string", 排序
 * @property {String} parentCode                 "string", 父级code
 * @property {String} code                       "string", 责任code
 * @property {String} name                       "string", 责任名称
 * @property {String} type                       "string", 显示类别 {1 对比责任, 2 投保条件, 3 保司信息}
 * @property {String} productCategory            "string",
 * @property {String} productCategoryName        "string",
 * @property {String} updateTime                 "2021-03-18T07:45:12.489Z"
 */

/** 责任对比接口 */
const ProductCompareAPI = {
  /**
   * 字典分页
   * @param {PaginationReq<CompareCategory>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<CompareCategory>>}
   */
  getDictList({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/compareCategory/getCompareCategoryList', { current, pageSize, ...reqData }, options);
  },

  /**
   * 字典保存
   * @param {CompareCategory} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<*>}
   */
  insertOrUpdateDict(data, options = null) {
    const url = data.id ? '/compareCategory/updateCompareCategory' : '/compareCategory/saveCompareCategory';
    return request.post(url, data, options);
  },

  deleteDict(id, options = null) {
    return request.get('/compareCategory/deleteCompareCategoryById', { ...options, params: { id } });
  },

  /**
   * 查询对比责任配置
   * @param {productCode} productCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<*>}
   */
  getBenefitsByCode(productCode, options = null) {
    return request.get('/compareBenefit/getCompareBenefitList', { ...options, params: { productCode } });
  },

  /**
   * 保存对比责任配置
   */
  saveBenefits(benefitList, productCode, options = null) {
    return request.post(
      '/compareBenefit/saveCompareBenefit',
      { productCode, compareBenefitList: benefitList },
      options,
    );
  },

  /**
   * 获取对比险种列表
   * @param {PaginationReq<{}>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<{}>>}
   */
  getCompareProductList({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/compareInsure/getCompareInsureList', { current, pageSize, ...reqData }, options);
  },

  /**
   * 更新对比险种 (启用/禁用)
   * @param {Object} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  updateCompareProduct(data, options = null) {
    return request.post('/compareInsure/updateCompareInsure', data, options);
  },

  /**
   * 删除对比险种
   * @param id
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  removeCompareProduct(id, options = null) {
    return request.get('/compareInsure/deleteCompareInsureById', { ...options, params: { id } });
  },
};

export default ProductCompareAPI;
