import request from '@/utils/request';

/**
 * @typedef {Object} OnSale
 * @property {String} id
 * @property {String} packageCode
 * @property {String} onSaleName
 * @property {String} packageStyle
 * @property {String} salemanAptitude
 * @property {String} organizationCode
 * @property {String} onSaleBeginTime
 * @property {String} onSaleEndTime
 * @property {String} createTime
 * @property {String} isRecommend
 * @property {String} onSaleStatus
 *
 * @property {String} ageAvg
 * @property {String} bigImgUrl
 * @property {String} companyCode
 * @property {String} companyName
 * @property {String} createUser
 * @property {String} imgUrl
 * @property {String} insurePlace
 * @property {String} insuredNum
 * @property {String} isElectricPin
 * @property {String} onsaleType
 * @property {String} saleBussessType
 * @property {String} saleChannel
 * @property {String} saleChannelSecond
 * @property {String} saleChannelThird
 * @property {String} updateTime
 * @property {String} updateUser
 */

/** 在售管理 */
const OnSaleAPI = {
  /**
   *
   * @param {PaginationReq<OnSale>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<OnSale>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/onsale/listProductsOnsale', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   * @param {String[]} idList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  remove(idList, options = null) {
    return request.post('/onsale/deletebyid', null, {
      ...options,
      params: { ids: idList.join(',') },
    });
  },

  /**
   *
   * @param {OnSale} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insertOrUpdate(data, options = null) {
    return request.post('/onsale/insertUpdateOnsale', data, options);
  },

  /**
   *
   * @param {String} onsaleId
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<OnSale>>}
   */
  getById(onsaleId, options = null) {
    return request.post('/onsale/productsOnsaleDetailsById', null, {
      ...options,
      params: { onsaleId },
    });
  },

  /**
   *
   * @param {String[]} packageCodesList
   */
  downloadFile(packageCodesList, options = null) {
    return request.get('/sql/migration', {
      hideMsg: true,
      ...options,
      params: { packageCodes: packageCodesList.join(',') },
    });
  },

  /**/
};

export default OnSaleAPI;
