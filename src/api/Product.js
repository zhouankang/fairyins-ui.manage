import request from '@/utils/request';

const ProductAPI = {
  /**
   * @typedef {Object} ProductItem                    产品列表
   * @property {String} businessCode                  "007"
   * @property {String} companyCode                   "tfqq"
   * @property {String} companyName                   null
   * @property {String} id                            "1278601533658759168"
   * @property {String} isAnnuityValue                "0"
   * @property {String} isCashValue                   "1"
   * @property {String} isExemptions                  "0"
   * @property {String} isOneYear                     "0"
   * @property {String} isPayable                     "0"
   * @property {String} isSurvivalBonus               "0"
   * @property {String} productAttribute              "traditionalRisk"
   * @property {String} productCategory               "MI"
   * @property {String} productCategorySecond         ""
   * @property {String} productCategoryThird          ""
   * @property {String} productCode                   "007"
   * @property {String} productName                   "皮皮主险"
   * @property {String} productNature                 "P"
   * @property {String} productState                  "0"
   * @property {String} productType                   "0"
   * @property {String} questCode
   *   "{"holderQuest":["N123","N124"],"insuredQuest":[],"agentQuest":[]}"
   * @property {String} renewableAge                  "60"
   * @property {String} renewableTerm                 null
   * @property {String} riskDiscount                  null
   * @property {String} saleChannel                   "A1,G1,D1,K1"
   * @property {Date} saleBeginTime                   "2020-07-01 00:00:00.0"
   * @property {Date} saleEndTime                     "2021-07-02 00:00:00.0"
   * @property {String} saleStateMark                 "1"
   * @property {String} timeRiskMark                  "L"
   */

  /**
   * @typedef {Object} ProductClause                  产品条款
   * @property {String} id
   * @property {Date} effectiveBeginTime              生效开始时间 ,
   * @property {Date} effectiveEndTime                生效结束时间 ,
   * @property {String} effectiveLongTerm             1 ,
   * @property {String} relationCode                  产品code ,
   * @property {String} termsDesc                     条款说明 ,
   * @property {String} termsName                     条款名称 ,
   * @property {String} termsType                     pdf、jpg、word ... ,
   * @property {String} termsUrl                      条款上传URL
   */

  /**
   * 分页
   * @param {PaginationReq<ProductItem>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<ProductItem>>}
   */
  getList({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/product/getProductList', { current, pageSize, ...reqData }, options);
  },

  /** 获取险种表单详情 */
  getInfo(productCode, options = null) {
    return request.post('/product/initProductInfo', null, { ...options, params: { productCode } });
  },

  getCopy(productCode, businessCode, options = null) {
    return request.get('/product/copy', { ...options, params: { productCode, businessCode } });
  },

  /** 获取险种详情 */
  getByCode(productCode, options = null) {
    return request.post('/product/getProductDetail', null, {
      ...options,
      params: { productCode },
    });
  },

  remove(idList, options = null) {
    return request.post('/product/deleteProductByIds', null, {
      ...options,
      params: { idList: idList.join(',') },
    });
  },

  /**
   *
   * @param {ProductItem} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<ProductItem[]>>}
   */
  selectAll({ notProposal = false, notCompare, ...data }, options = null) {
    let url = '/product/selectProductAll';
    if (notProposal) url = '/product/selectProductByNotProposal';
    if (notCompare) url = '/product/selectProductByNotCompare';
    return request.post(url, data, options);
  },

  /**
   *
   * @param data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insertOrUpdate(data, options = null) {
    return request.post('/product/complete', data, options);
  },

  /**
   *
   * @param {String[]} prodCodeList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  selectProductDetailList(prodCodeList, options = null) {
    return request.post(
      '/product/selectProductDetailList',
      prodCodeList.map((v) => ({ productCode: v, salesProductCode: v })),
      options,
    );
  },

  /**
   * 获取条款
   * @param {String} relationCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<Array<ProductClause>>>}
   */
  getClause(relationCode, options = null) {
    return request.get('/ProductClauseApi/selectClause', { ...options, params: { relationCode } });
  },

  /**
   * 新增/修改条款
   * @param {Array<ProductClause>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<Array<ProductClause>>>}
   */
  updateClause(data, options = null) {
    return request.post('/ProductClauseApi/insertOrUpdate', data, options);
  },

  /**
   * @param {Array<String>} idList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  deleteClause(idList, options = null) {
    return Promise.all(idList.map((id) => request.get('/ProductClauseApi/del', { ...options, params: { id } })));

    return request.get('/ProductClauseApi/del', { ...options, params: { id: idList.join(',') } });
  },

  updateInfoStatus(productId, attachmentStatus, options = null) {
    return request.post('/product/saveProductTable', { id: productId, attachmentStatus });
  },
};

export default ProductAPI;
