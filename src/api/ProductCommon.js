import request from '@/utils/request';

const ProductCommon = {
  /**
   * 验证公式
   * @param {String} formula
   * @param {String} type
   * @param {AxiosRequestConfig} [options]
   * @return {Promise<ApiResp<String>>}
   */
  validateFormula(formula, type, options = null) {
    return request.post('/verification/validateFunction', { functionTemplateList: [formula], type: 'm' }, options);
  },

  /** 费用管理 */
  costManager: {
    /**
     * @typedef {Object} Cost             费用描述对象
     * @property {String} id
     * @property {String} costName
     * @property {String} costCode
     * @property {String} costObject
     * @property {String} createDate
     */

    /**
     * 录入对象
     * @param {AxiosRequestConfig} [options]
     * @return {Promise<ApiResp<{ [string]: Cost}>>}
     */
    getWriteObject(options = null) {
      return request.get('/CostManageController/writeObject', null, options);
    },
  },

  /**
   * 上传文件
   * @param {Array<File>} files
   * @param {AxiosRequestConfig} options
   * @returns {Promise<ApiResp>}
   */
  uploadFiles(files, options = null) {
    const formData = new FormData();
    [].forEach.call(files, (file, index) => {
      formData.append(`file${index}`, file);
    });
    return request.post('/UploadAttachmentController/uploadFiles', formData, options);
  },
};

export default ProductCommon;
