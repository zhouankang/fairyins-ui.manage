import request from '@/utils/request';

const FormulaAPI = {
  /**
   * @typedef {Object} Formula            公式对象
   * @property {String} id
   * @property {String} code              code
   * @property {String} formula           公式
   * @property {String} formulaDesc       desc
   * @property {String} formulaMapping    mapping
   * @property {String} formulaType       type
   * @property {String} createDate        创建时间
   * @property {Factor[]} factorPoolList  因子列表
   */

  /**
   * @param {PaginationReq<Formula>} data
   * @param {AxiosRequestConfig} [options]
   * @return {Promise<PaginationResp<Formula>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/formulaPool/formulaPoolFindPage', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   * @param {String} relationCode
   * @param options
   * @param {AxiosRequestConfig} [options]
   * @return {Promise<ApiResp<{ FormulaAndFactorPoolVo: { formulaPool: Formula, list:
   *   Array<Formula>, Attachment }}>>}
   */
  findByCode(relationCode, options = null) {
    return request.post('/factorPool/findByCodeAndType', null, {
      ...options,
      params: { relationCode },
    });
  },
};

export default FormulaAPI;
