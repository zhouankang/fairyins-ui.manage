import request from '@/utils/request';

const AttributePoolAPI = {
  findByPage({ _, current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/attributepool/page', { current, pageSize, ...reqData }, options);
  },

  remove(id, code, options = null) {
    return request.get('/attributepool/del', { params: { id, code }, ...options });
  },

  insertOrUpdate(data, options = null) {
    return request.post('/attributepool/insertOrUpdate', [data], options);
  },
};

export default AttributePoolAPI;
