import request from '@/utils/request';

/** 因子库 */
const FactorAPI = {
  /**
   * @typedef {Object} Factor           因子对象
   * @property {String} id
   * @property {String} cname           名称
   * @property {String} code            code
   * @property {String} factorDesc      描述
   * @property {String} factorObject    对象
   * @property {String} createTime      创建时间
   * @property {String} javaType        Java类型
   */

  /**
   * 分页
   * @param {PaginationReq<Factor>} data
   * @param {AxiosRequestConfig} [options]
   * @return {Promise<PaginationResp<Factor>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/factorPool/factorPoolFindPage', { current, pageSize, ...reqData }, options);
  },

  /**
   * 新增因子对象
   * @param {String} name
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  addFactorObject(code, cnName, options = null) {
    return request.post('/dictionary/insertDictionary', {
      code,
      cnName,
      parentId: '1401112690121768960',
      rootId: '1401112690121768960',
    });
  },

  /**
   * 新增/修改因子
   * @param {Factor} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insertOrUpdate(data, options = null) {
    return request.post('/factorPool/insertOrUpdate', [data], options);
  },

  remove(idList = [], options = null) {
    return request.post('/factorPool/deleteByIds', null, {
      ...options,
      params: { idList: idList.join(',') },
    });
  },
};

export default FactorAPI;
