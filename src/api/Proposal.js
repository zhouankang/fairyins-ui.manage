import request from '@/utils/request';

/**
 * @typedef {Object} ProductDefine
 * @property {String}   id                    "string",
 * @property {String}   combinationType       "1",
 * @property {String}   productCode           "GADD",
 * @property {String}   productDetail         "此款产品...",
 * @property {String}   productName           "意外伤害险",
 * @property {String}   productShort          "意外险",
 * @property {String}   productType           "0",
 * @property {String}   saleChannel           "直销"
 * @property {String}   createTime            "2020-10-12T03:07:54.262Z",
 * @property {String}   gapClassification     "PE",
 * @property {String}   imgUrl                "/img/a.jpg",
 * @property {String}   proposalModel         "text",
 */

/**
 * @typedef {Object} RiskAmountFormula
 * @property {String} id               "string"
 * @property {String} formulaName      "string"
 * @property {String} computeFormula   "string"
 * @property {String} packageCode      "1"
 * @property {String} packageName      "组合1"
 * @property {String} planCode         "1"
 * @property {String} planName         "计划1"
 * @property {String} productCode      "string"
 * @property {String} productName      "团体意外医疗"
 * @property {String} remarks          "string"
 * @property {String} riskMarkType     "string"
 * @property {String} riskType         "string"
 */

/**
 * @typedef {Object} DefinitionPayload
 * @property {Array<ProductDefine>}   productDefineList
 * @property {Array<RiskAmountFormula>}   riskAmountFormulaVVoList
 */

/**
 * @typedef {Object} ProductInfo
 * @property {String} id                     "1285270528411566080"
 * @property {String} attachmentType         null
 * @property {String} attachmentUrl          null
 * @property {String} combinationType        "1"
 * @property {String} contentType            描述类型  1 富文本; 2 附件;
 * @property {String} createTime             null
 * @property {String} isShow                 "1"
 * @property {String} relationCode           "ceshi"
 * @property {String} richTextInfo           富文本内容， 当contentType = 2时，作为附件文件名使用
 * @property {String} showContentTitle       定义showContentType的显示名称
 * @property {String} showContentType        描述key (e.g. "PI" "PF" "PDF_HEAD")
 * @property {String} updateTime             null
 */

/** */
const ProposalAPI = {
  /**
   * @typedef {Object} Proposal
   * @property {String} id:                   "1287761481173041152"
   * @property {String} productCode:          "1901"
   * @property {String} productName:          "招商信诺招盈二号（2018）年金保险（分红型）保险产品计划"
   * @property {String} productShort:         "招盈二号（2018）"
   * @property {String} productType:          "P"
   * @property {String} createTime:           "2020-07-27"
   * @property {String} combinationType:      "2"
   * @property {String} gapClassification:    null
   * @property {String} imgUrl:               null
   * @property {String} productDetail:        "短期投入 终身领取 稳定给付↵可享分红 万能账户 锦上添花↵↵↵"
   * @property {String} proposalModel:        null
   * @property {String} saleChannel:          null
   */

  /**
   *
   * @param {PaginationReq<Proposal>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<Proposal>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...pagerData }, options = null) {
    return request.post('/ProposalManagement/productDefineFindPage', { current, pageSize, ...pagerData }, options);
  },

  /**
   *
   * @param {Proposal.id[]} idList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  remove(idList, options = null) {
    return request.post('/ProposalManagement/deleobj', null, {
      ...options,
      params: { ids: idList.join(','), type: 1 },
    });
  },

  /**
   *
   * @param {String} Id
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<DefinitionPayload>>}
   */
  byId(Id, options = null) {
    return request.post('/ProposalManagement/selectProductDefineById', null, {
      ...options,
      params: { Id },
    });
  },

  /**
   *
   * @param {String} Code               Product.id
   * @param {String} [combinationType]  组合类型
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<DefinitionPayload>>}
   */
  byCode({ Code, combinationType = '1' }, options = null) {
    return request.post('/ProposalManagement/selectProductDefineByProductCode', null, {
      ...options,
      params: { Code, combinationType },
    });
  },

  /**
   *
   * @param {DefinitionPayload} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insert(data, options = null) {
    return request.post('/ProposalManagement/insertProductDefineByProductCode', data, options);
  },

  /**
   *
   * @param {String} Code               Product.id
   * @param {String} [combinationType]  组合类型
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<ProductInfo[]>>}
   */
  getProductInfo({ Code, combinationType = '1' }, options = null) {
    return request.post('/ProposalManagement/selectProductInfoByCodeid', null, {
      ...options,
      params: { Code, combinationType },
    });
  },

  /**
   *
   * @param {ProductInfo[]} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insertProductInfo(data, options = null) {
    return request.post('/ProposalManagement/InsertProductInfoByCodeid', data, options);
  },
};
export default ProposalAPI;
