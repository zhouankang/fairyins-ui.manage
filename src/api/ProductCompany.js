import request from '@/utils/request';

/**
 *
 * @typedef {Object} ProductCompanyItem                    产品列表
 * @property {string} id
 * @property {string} abbreviationName                     公司名称缩写 ,
 * @property {string} actual                               实际控制人 ,
 * @property {string} annualCompensation                   全年赔付件数 ,
 * @property {string} annualRatio                          理赔赔付率 ,
 * @property {string} beforeLastClaims                     理赔总额 ,
 * @property {string} beforeLastLimitation                 理赔时效 ,
 * @property {string} beforeLastYear                       前年赔付率年度 ,
 * @property {string} cname                                公司中文名称 ,
 * @property {string} code                                 公司编码 ,
 * @property {string} companyTel                           公司客服电话 ,
 * @property {string} companyWeb                           公司官网地址 ,
 * @property {string} ename                                公司英文名称 ,
 * @property {string} foundedTime                          公司成立时间 ,
 * @property {string} lastClaims                           理赔总额 ,
 * @property {string} lastLimitation                       理赔时效 ,
 * @property {string} lastYear                             去年赔付率年度 ,
 * @property {string} nature                               保险公司性质 ,
 * @property {string} registeredCapital                    公司注册资本 ,
 * @property {string} riskRating                           风险综合评级 ,
 * @property {string} sarmra                               SARMRA评分 ,
 * @property {string} shareholders                         公司股东情况 ,
 * @property {string} shareholdersChanges                  公司股东变更情况 ,
 * @property {string} solvency                             保险公司偿付能力
 * @property {Array<ProductInfo>} productInfos             运营服务附件, relationCode为保司编码, contentType:
 *   SF 投保流程，PP 保全流程，CF 理赔流程
 */

const ProductCompanyAPI = {
  /**
   * 分页
   * @param {PaginationReq<ProductCompanyItem>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<ProductCompanyItem>>}
   */
  getList({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/product/company/pageInfo', { current, pageSize, ...reqData }, options);
  },

  /**
   * @param {ProductCompanyItem} data
   * @param {AxiosRequestConfig} options
   * @returns {Promise<ProductCompanyItem>}
   */
  insertOrUpdate(data, options = null) {
    return request.post('/product/company/insertOrUpdate', data, options);
  },

  remove(data, options = null) {
    return request.post('/product/company/deleteCompany', data, options);
  },
};

export default ProductCompanyAPI;
