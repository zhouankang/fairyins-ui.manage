import request from '@/utils/request';

const PackageAPI = {
  /**
   *
   * @param {PaginationReq} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/groupPackageController/getPackageList', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   * @param {PaginationReq} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp>}
   */
  listForOnSale({ current = 1, pageSize = 999, ...reqData }, options = null) {
    return request.post('/groupPackageController/getPackageListForOnSale', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   */
  remove(idList, options = null) {
    return request.post('/groupPackageController/deletePackage', null, {
      ...options,
      params: { packageCodes: idList.join(',') },
    });
  },

  /**
   *
   * @param {Object} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  svaOrUpdate(data, options = null) {
    return request.post('/groupPackageController/saveOrUpdate', data, options);
  },

  /**
   *
   * @param {PaginationReq<Plan>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<Plan>>}
   */
  selectPlans({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/groupPackageController/getProductPlanList', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   * @param {String} packageCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  getByCode(packageCode, options = null) {
    return request.post('/groupPackageController/selectGroupDetails/', null, {
      ...options,
      params: { packageCode },
    });
  },
};

export default PackageAPI;
