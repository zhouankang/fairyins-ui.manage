import { clearDictByCodes } from '@/components';
import request from '@/utils/request';
import { message } from 'antd';

/**
 * @typedef {Object} DictionaryItem
 * @property {String} id                  主键
 * @property {String} code                字典code
 * @property {String} cnName              中文名称
 * @property {String} enName              英文名称
 * @property {String} value               字典取值
 * @property {String} rootId              父字典的id
 * @property {Number} layer               节点层级
 * @property {Number} sort                同层级排序
 * @property {String} description         字典描述
 * @property {String} enabled             是否可用
 * @property {Number} systemFlag          字典类型
 * @property {String} createById          创建者id
 * @property {String} createByName        创建者名称
 * @property {Date}   createTime          创建时间
 * @property {String} updateById          更新者id
 * @property {String} updateByName        修改者名称
 * @property {Date}   updateTime          修改时间
 */

const onDone = (fn) => (res) => {
  clearDictByCodes();
  return fn?.(res) ?? res;
};

/** 字典接口 */
const DictionaryAPI = {
  /**
   *
   * @param {PaginationReq<DictionaryItem>}   data
   * @param {AxiosRequestConfig}              options
   * @return {Promise<PaginationResp<DictionaryItem>>}
   */
  findByPage({ current = 1, pageSize = 9999, sortParams = null, ...reqData }, options = null) {
    return request.post(
      '/dictionary/list',
      {
        current,
        pageSize,
        sortParams: {
          sort: true,
          ...sortParams,
        },
        ...reqData,
      },
      options,
    );
  },

  /**
   *
   * @param {AxiosRequestConfig} options
   * @return {Promise<DictionaryItem[]>}
   */
  getRoot(options = null) {
    return request.get('/dictionary/root/list', { hideMsg: true, dontThrow: true, ...options });
  },

  /**
   *
   * @param {String} id
   * @param {AxiosRequestConfig} options
   * @return {Promise<DictionaryItem>}
   */
  getById(id, options = null) {
    return request.get('/dictionary/find', {
      hideMsg: true,
      dontThrow: true,
      ...options,
      params: { id },
    });
  },

  /**
   *
   * @param {DictionaryItem} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  update(data, options = null) {
    return request.post('/dictionary/update', data, { ...options, hideMsg: true, dontThrow: true }).then(
      onDone((res) => {
        if (!res) {
          message.error('保存失败');
          return Promise.reject({ data: false, code: '100', msg: '保存失败' });
        }
        return { data: true, code: '000', msg: '保存成功' };
      }),
    );
  },

  /**
   *
   * @param {DictionaryItem} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insert(data, options = null) {
    return request.post('/dictionary/insertDictionary', data, options).then(onDone());
  },

  /**
   *
   * @param {DictionaryItem.id[]} idList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  remove(idList, options = null) {
    return request
      .post('/dictionary/batch/delete', null, {
        ...options,
        hideMsg: true,
        dontThrow: true,
        params: { id: idList.join(',') },
      })
      .then(onDone());
  },
};

export default DictionaryAPI;
