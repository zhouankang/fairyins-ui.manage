import request from '@/utils/request';

const DiseaseAPI = {
  findByPage({ _, current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/ProductDisease/pageList', { current, pageSize, ...reqData }, options);
  },

  insertOrUpdate(data, options = null) {
    return request.post('/ProductDisease/insertOrUpdate', data, options);
  },

  getByProductCode(productCode, options = null) {
    return request.post('/ProductDisease/view', null, { params: { productCode }, ...options });
  },

  remove(productCode, options = null) {
    return request.post('/ProductDisease/del', { productCode }, options);
  },

  removeItem(id, options = null) {
    return request.post('/ProductDisease/del', { id }, options);
  },
};

export default DiseaseAPI;
