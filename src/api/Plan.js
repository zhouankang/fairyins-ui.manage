import request from '@/utils/request';

/**
 * @typedef {Object} Plan
 * @property {String} id
 * @property {String} planCode
 * @property {String} planName
 * @property {String} planState
 * @property {String} planType
 * @property {String} saleBeginTime
 * @property {String} saleEndTime
 * @property {String} saleStateMark
 * @property {String} companyCode
 * @property {String} companyName
 * @property {String} num
 * @property {String} coreCode
 * @property {String} createTime
 * @property {String} planBusinessCode
 * @property {String} productNature
 * @property {String} remarks
 * @property {String} saleChannel
 *
 * @property {ProductItem[]} products
 */

/** 计划管理 */
const PlanAPI = {
  /**
   *
   * @param {PaginationReq<Plan>} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<PaginationResp<Plan>>}
   */
  findByPage({ current = 1, pageSize = 9999, ...reqData }, options = null) {
    return request.post('/PlanController/getPlanList', { current, pageSize, ...reqData }, options);
  },

  /**
   *
   * @param {String[]} idList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  remove(idList, options = null) {
    return request.post('/PlanController/deletePlan', null, {
      ...options,
      params: { ids: idList.join(',') },
    });
  },

  /**
   *
   * @param {String} planBusinessCode
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  getByCode(planBusinessCode, options = null) {
    return request.post('/PlanController/view', null, { ...options, params: { planBusinessCode } });
  },

  /**
   *
   * @param {Object} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  insertOrUpdate(data, options = null) {
    return request.post('/PlanController/saveOrUpdate', data, options);
  },
};

export default PlanAPI;
