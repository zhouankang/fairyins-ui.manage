import request from '@/utils/request';

/**
 * @typedef {Object} BenefitTreeItem
 * @property {String} id
 * @property {Array<BenefitTreeItem>} children
 * @property {String} parentCode
 * @property {String} benefitCode
 * @property {String} benefitDesc
 * @property {String} benefitFormula
 * @property {String} benefitName
 * @property {String} benifitIcon
 * @property {String} benifitsort
 * @property {String} factorFormula
 * @property {String} isShow
 * @property {String} layer
 * @property {String} merge
 * @property {String} planCode
 * @property {String} productCode
 * @property {String} alias
 * @property {String} sign
 * @property {String} on
 * @property {String} type
 */

const BenefitAPI = {
  /**
   *
   * @param planCode
   * @param parentCode
   * @param type
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp<BenefitTreeItem[]>>}
   */
  getTree({ planCode, parentCode, type = 0 }, options = null) {
    return request.post('/benifit/getBenefitTreeList', null, {
      ...options,
      params: { planCode, parentCode, type },
    });
  },

  /**
   * 新增节点
   * @param {BenefitTreeItem} data
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  add(data, options = null) {
    return request.post('/benifit/addBenefitTreeList', [data], options);
  },

  /**
   * 删除节点
   * @param {Array<BenefitTreeItem.id>} idList
   * @param {AxiosRequestConfig} options
   * @return {Promise<ApiResp>}
   */
  remove(idList, options = null) {
    return request.post('/benifit/deleteBenefitTreeList', idList, options);
  },

  batchUpdate(data, options = null) {
    return request.post('/benifit/addBenefitTreeList', data, options);
  },
};

export default BenefitAPI;
